﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core;
using log4net;
using Bot.Core.Enumerations;

namespace Bot.UI
{
    public class PokerActionChoices
    {
        ILog _log = LogManager.GetLogger(typeof(PokerActionChoices));
        List<PokerAction> _actions = new List<PokerAction>();
        public void AddChoice(string choice)
        {
            //parse out the amount
            var chunks = choice.Split(' ');
            var numberAsString = chunks[chunks.Length - 1];
            double number;
            if (double.TryParse(numberAsString, out number))
            {
                if(choice.Contains('c') || choice.Contains('C'))
                {
                    var actionChoice = PokerActionType.None;
                    if (number == 0)
                    {
                        actionChoice = PokerActionType.Check;
                        _actions.Add(new PokerAction(actionChoice, number));
                    }
                    else
                    {
                        actionChoice = PokerActionType.Call;
                        _actions.Add(new PokerAction(actionChoice, number));
                    }
                }
            }
        }
        public IEnumerable<PokerAction> GetChoices()
        {
            return _actions;
        }
    }
}
