﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using log4net.Config;
using log4net;
using System.Windows.Threading;

namespace Bot.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static ILog _log = LogManager.GetLogger("InterceptedDataLogger");
        public static string[] Args;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // BasicConfigurator replaced with XmlConfigurator.
            XmlConfigurator.Configure();
            Console.ForegroundColor = ConsoleColor.Yellow;
            

            Args = e.Args;

            HookHandler.MyEvent += new MyEventHandler(HookHandler_MyEvent);
        }

        void HookHandler_MyEvent(object sender, MyEventArgs e)
        {
            _log.Debug(e.Line);
        }
        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            LogManager.GetLogger(typeof(App)).Fatal(e.Exception.Message, e.Exception);
            e.Handled = true;
        }
    }
}
