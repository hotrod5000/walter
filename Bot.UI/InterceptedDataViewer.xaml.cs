﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using log4net;

namespace Bot.UI
{
    /// <summary>
    /// Interaction logic for InterceptedDataViewer.xaml
    /// </summary>
    public partial class InterceptedDataViewer : Window
    {
        public InterceptedDataViewer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HookHandler.MyEvent += new MyEventHandler(HookHandler_MyEvent);
            
        }
     

        private void Window_Closed(object sender, EventArgs e)
        {
            HookHandler.MyEvent -= new MyEventHandler(HookHandler_MyEvent);
        }

        void HookHandler_MyEvent(object sender, MyEventArgs e)
        {
            
            RichTextBox1.Dispatcher.Invoke(new Action(
                delegate()
                {
                    Paragraph para = new Paragraph();

                    para.Inlines.Add(new Run(e.Line));
                    RichTextBox1.Document.Blocks.Add(para);
                    RichTextBox1.ScrollToEnd();

                }));
            
            
        }


    }
}
