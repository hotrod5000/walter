﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core;
using Bot.Core.Interfaces;
using Bot.Core.Objects;
using log4net;


namespace Bot.UI
{
    public class MyEventArgs : EventArgs
    {
        public MyEventArgs(string lineWritten)
        {
            Line = lineWritten;
        }
        public string Line { get; set; }
    }
    public delegate void MyEventHandler(object sender, MyEventArgs e);

    public class HookHandler : MarshalByRefObject
    {
        static public bool Running = false;
        static ILog _log = LogManager.GetLogger(typeof(HookHandler));
        static Dictionary<IntPtr, HoldemHand> _hands = new Dictionary<IntPtr, HoldemHand>();
        static Dictionary<IntPtr, PokerActionChoices> _actions = new Dictionary<IntPtr, PokerActionChoices>();
        static IntPtr _mostRecentTableCardsWereDealtToMe;
        static IntPtr _mostRecentTableActionWasRequestedOfMe;
        static Queue<string> _buffer = new Queue<string>();
        static bool _cardsBeingDealt = false;
        static string _dealerPos = string.Empty;
        static GameModel _gameModel;
        public static event MyEventHandler MyEvent;
        private bool _collectingActionChoices;
        public HookHandler()
        {

        }
        public void IsInstalled(Int32 InClientPID)
        {
            _log.Debug(string.Format("ConsoleApplication1Interface has been installed in target {0}.\r\n", InClientPID));
        }
        protected virtual void OnMyEvent(MyEventArgs e)
        {
            if(MyEvent!=null)
                MyEvent(this, e);
        }

        public void OnFileWrite(Int32 InClientPID, IntPtr fileHandle, String[] info)
        {
            if (fileHandle == new IntPtr(7)) return; //this is a hacky way to determine if this is a console write from fileWriter 
            if (!Running)
            {
                return;
            }
            foreach (var s in info)
            {
                OnMyEvent(new MyEventArgs(s));
                string item = s.Trim();
                if (item.Contains("MSG_TABLE_LOGIN_REPLY"))
                {
                    var chunks = item.Split(' ');
                    //get table handle
                    string handle = chunks[1];
                    var intHandle = Int64.Parse(handle, System.Globalization.NumberStyles.HexNumber);
                    IntPtr p = new IntPtr(intHandle);
                    //get seat number
                    string sit = chunks[2];
                    int seatNumber = -1;
                    int.TryParse(sit.Substring(sit.Length - 1), out seatNumber);

                    //sit at table
                    TableManager.Instance.SitAtTable(p, seatNumber);

                }
                else if (item.Contains("MSG_TABLE_SUBSCR_DEALPLAYERCARDS"))
                {
                    _cardsBeingDealt = true;
                    _gameModel = new GameModel();
                    _gameModel.Initialize();
                }
                else if (_cardsBeingDealt && (item.Contains("sit")))
                {
                    if (_cardsBeingDealt)
                        _gameModel.ProcessMessage(item);
                }
                else if (item.Contains("dealerPos"))
                {
                    if (_cardsBeingDealt)
                    {
                        _gameModel.ProcessMessage(item);
                        _gameModel.Timestamp = DateTime.Now;
                        TableManager.Instance.ReceiveGameModel(_gameModel);
                        _cardsBeingDealt = false;
                    }
                }
                else if (item.Contains("MSG_TABLE_PLAYERCARDS"))
                {
                    //parse the window handle
                    string handle = item.Split(' ')[1];
                    var intHandle = Int64.Parse(handle, System.Globalization.NumberStyles.HexNumber);
                    IntPtr p = new IntPtr(intHandle);
                    _mostRecentTableCardsWereDealtToMe = p;
                    var hand = new HoldemHand() { TableWindowHandle = _mostRecentTableCardsWereDealtToMe };
                    hand.HandDealt += new HandDealtEventHandler(hand_HandDealt);
                    _hands[p] = hand;
                }
                else if (item.Contains(":::"))
                {
                    if (_hands.ContainsKey(_mostRecentTableCardsWereDealtToMe))
                    {
                        _hands[_mostRecentTableCardsWereDealtToMe].AddCard(item.Split(' ')[1]);

                    }
                }
                else if (item.Contains("MSG_TABLE_REQUESTACTION"))
                {
                    
                    //parse the window handle
                    try
                    {
                        string handle = item.Split(' ')[1];
                        var intHandle = Int64.Parse(handle, System.Globalization.NumberStyles.HexNumber);
                        IntPtr p = new IntPtr(intHandle);
                        _mostRecentTableActionWasRequestedOfMe = p;
                        _collectingActionChoices = true;
                        _actions[_mostRecentTableActionWasRequestedOfMe] = new PokerActionChoices();
                    }
                    catch (Exception e)
                    {
                        if (item.Contains("REPLY"))
                        {
                            TableManager.Instance.RequestAction(_mostRecentTableActionWasRequestedOfMe,_actions.ContainsKey(_mostRecentTableActionWasRequestedOfMe)?
                                _actions[_mostRecentTableActionWasRequestedOfMe].GetChoices(): new List<PokerAction>());
                            _collectingActionChoices = false;
                        }
                        else
                        {
                            _log.Error("Exception caught handling MSG_TABLE_REQUESTACTION message", e);
                        }
                    }

                }
                else if(_collectingActionChoices && _actions.ContainsKey(_mostRecentTableActionWasRequestedOfMe) &&
                        (item.Contains("'C'") ||
                        item.Contains("'c'") ||
                         item.Contains("'F'") ||
                        item.Contains("'*'")))
                {
                    _actions[_mostRecentTableActionWasRequestedOfMe].AddChoice(item);                    
                }
                else if (item.Contains("Statistics"))
                {
                    try
                    {
                        string handle = item.Split(' ')[3];
                        var intHandle = Int64.Parse(handle, System.Globalization.NumberStyles.HexNumber);
                        IntPtr p = new IntPtr(intHandle);
                        TableManager.Instance.Reset(p);

                    }
                    catch (Exception e) 
                    {
                        _log.Error(string.Format("Failed to get parse table handle from message '{0}'", item), e);
                    }
                }                        
            }
        }

        void hand_HandDealt(object sender, HandDealtEventArgs e)
        {
            if (_hands.ContainsKey(e.WindowHandle))
                _hands.Remove(e.WindowHandle);

           

            //create the game model
            TableManager.Instance.NewHandReceived(e.WindowHandle, new List<IPokerCard> { new PokerCard(e.Card1), new PokerCard(e.Card2) });


        }
        
        public void ReportException(Exception InInfo)
        {
            _log.Debug("xxx:" + InInfo.Message);
            _log.Debug("The target process has reported an error:\r\n" + InInfo.ToString());
        }

        public void Ping()
        {
        }
    }
}
