﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using ManagedWinapi.Windows;
using ManagedWinapi.Windows.Contents;
using Bot.Core.Objects;
using Bot.Core;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using log4net;
using Size = System.Drawing.Size;
using System.Threading;

namespace Bot.UI
{
    /// <summary>
    /// Interaction logic for Screenshot.xaml
    /// </summary>
    public partial class Screenshot : Window
    {
        protected ILog _log = LogManager.GetLogger(typeof(Screenshot));
        ScreenCalibration _calibration;
        public Screenshot()
        {
            InitializeComponent();
        }

        
        SystemWindow _window;
        public Screenshot(SystemWindow w) : this()
        {
            _window = w;
            //var style = w.Style;
            //var xStyle = w.ExtendedStyle;
            //var windowState = w.WindowState;
            //w.Style &= ~(WindowStyleFlags.BORDER | WindowStyleFlags.CAPTION| WindowStyleFlags.THICKFRAME | WindowStyleFlags.MINIMIZE | WindowStyleFlags.MAXIMIZE | WindowStyleFlags.SYSMENU );
            //w.ExtendedStyle &= ~(WindowExStyleFlags.DLGMODALFRAME | WindowExStyleFlags.CLIENTEDGE | WindowExStyleFlags.STATICEDGE);
            //w.Style = 0;
            //w.ExtendedStyle =0;
            //w.WindowState = System.Windows.Forms.FormWindowState.Normal;
            //RECT r = w.Position;
            //r.Left = 0;
            //w.Position = r;
            System.Drawing.Image i = w.Image;
            // ImageSource ...

            BitmapImage bi = new BitmapImage();

            bi.BeginInit();

            MemoryStream ms = new MemoryStream();

            // Save to a memory stream...

            i.Save(ms, ImageFormat.Bmp);

            // Rewind the stream... 

            ms.Seek(0, SeekOrigin.Begin);

            // Tell the WPF image to use this stream... 

            bi.StreamSource = ms;

            bi.EndInit();
            
            
            _im.Height = i.Height;
            _im.Width = i.Width;
            _im.Source = bi;
            _im.Opacity = 0.5;
            
            _im.MouseUp += new MouseButtonEventHandler(_im_MouseUp);
            
            //w.Style = style;
            //w.ExtendedStyle = xStyle;
            //w.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            //w.WindowState = windowState;
            Title = string.Format("{0} - Calibration", w.Title);
        }

        System.Windows.Point _point;
        void _im_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
            _point = e.GetPosition(_im);
            return;

            
        }

        private void Fold_Click(object sender, RoutedEventArgs e)
        {
            TableController c = TableManager.Instance.GetController(_window.HWnd);
            if(c!=null)
            {
                var point = new Bot.Core.POINT((int)_point.X, (int)_point.Y);
                c.SetScreenCoordinates(Core.Enumerations.ScreenItems.FoldCheckBox,point);
                _calibration.SetScreenCoordinates(Core.Enumerations.ScreenItems.FoldCheckBox, point);
                _log.Debug(string.Format("Setting screen coordinates for Fold Check Box to {0}",point));
            }
        }

        private void SliderLeftPos_Click(object sender, RoutedEventArgs e)
        {
            TableController c = TableManager.Instance.GetController(_window.HWnd);
            if (c != null)
            {
                var point = new Bot.Core.POINT((int)_point.X, (int)_point.Y);
                c.SetScreenCoordinates(Core.Enumerations.ScreenItems.SliderLeftPosition,point);
                _calibration.SetScreenCoordinates(Core.Enumerations.ScreenItems.SliderLeftPosition, point);
                _log.Debug(string.Format("Setting screen coordinates for SliderLeftPosition to {0}", point));
            }
        }

        private void Raise_Click(object sender, RoutedEventArgs e)
        {
            TableController c = TableManager.Instance.GetController(_window.HWnd);
            if (c != null)
            {
                var point = new Bot.Core.POINT((int)_point.X, (int)_point.Y);
                c.SetScreenCoordinates(Core.Enumerations.ScreenItems.RaiseButton, point);
                _calibration.SetScreenCoordinates(Core.Enumerations.ScreenItems.RaiseButton, point);
                _log.Debug(string.Format("Setting screen coordinates for RaiseButton to {0}", point));
            }
        }

        private void SaveAsDefault_Click(object sender, RoutedEventArgs e)
        {
            SerializationMethods.SerializeScreenCalibration(_calibration);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _calibration = new ScreenCalibration(new Size((int)_im.Width,(int) _im.Height));

        }

        private void FoldButton_Click(object sender, RoutedEventArgs e)
        {
            TableController c = TableManager.Instance.GetController(_window.HWnd);
            if (c != null)
            {
                var point = new Bot.Core.POINT((int)_point.X, (int)_point.Y);
                c.SetScreenCoordinates(Core.Enumerations.ScreenItems.FoldButton, point);
                _calibration.SetScreenCoordinates(Core.Enumerations.ScreenItems.FoldButton, point);
                _log.Debug(string.Format("Setting screen coordinates for Fold Button to {0}", point));
            }
        }

        private void Check_Click(object sender, RoutedEventArgs e)
        {
            TableController c = TableManager.Instance.GetController(_window.HWnd);
            if (c != null)
            {
                var point = new Bot.Core.POINT((int)_point.X, (int)_point.Y);
                c.SetScreenCoordinates(Core.Enumerations.ScreenItems.CheckButton, point);
                _calibration.SetScreenCoordinates(Core.Enumerations.ScreenItems.CheckButton, point);
                _log.Debug(string.Format("Setting screen coordinates for Check Button to {0}", point));
            }
        }
        private void ChatWindowPos_Click(object sender, RoutedEventArgs e)
        {
            TableController c = TableManager.Instance.GetController(_window.HWnd);
            if (c != null)
            {
                var point = new Bot.Core.POINT((int)_point.X, (int)_point.Y);
                c.SetScreenCoordinates(Core.Enumerations.ScreenItems.ChatWindow, point);
                _calibration.SetScreenCoordinates(Core.Enumerations.ScreenItems.ChatWindow, point);
                _log.Debug(string.Format("Setting screen coordinates for Chat Window to {0}", point));
            }
        }
        

    }
}
