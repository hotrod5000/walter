﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasyHook;
using System.Runtime.Remoting;

using System.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Collections.ObjectModel;
using Bot.Core;
using Bot.Core.Objects;
using System.ComponentModel;
using ManagedWinapi.Windows;
using HoldemStartingHandSelector;
using log4net;
using Bot.UI.Properties;
using System.IO;
using Bot.Core.Interfaces;

namespace Bot.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IGlobalRunState, INotifyPropertyChanged
    {
        ILog _log;
        BackgroundWorker _worker = new BackgroundWorker();
        ObservableCollectionEx<TableController> _tableCollection = new ObservableCollectionEx<TableController>();

        public ObservableCollectionEx<TableController> TableCollection
        {
            get
            {

                return _tableCollection;
            }
        }


        public MainWindow()
        {
            _log = LogManager.GetLogger(typeof(MainWindow));
            _log.Debug("Initializing...");
            _log.Debug(Assembly.GetExecutingAssembly().Location);
            
            
            InitializeComponent();
            TableManager.Instance.GlobalRunState = this;
            TableManager.Instance.TableAdded += new EventHandler<TableCollectionModifiedEventArgs>(Instance_TableAdded);
            _worker.DoWork += new DoWorkEventHandler(_worker_DoWork);

            
            
        }

        void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int processId = (int)e.Argument;
            while (true)
            {
                
                //every n seconds, find open windows
                var tables = SystemWindow.FilterToplevelWindows(x => x.Process.Id == processId &&
                                                               x.Parent.HWnd == new IntPtr(0) &&
                                                               x.Visible &&
                                                               !x.Title.Contains("Lobby"));

                TableManager.Instance.Synchronize(tables);
                
                Thread.Sleep(5000);
                
            }
        }

        void Instance_TableAdded(object sender, TableCollectionModifiedEventArgs e)
        {
            //_tableCollection.Clear();
            while (_tableCollection.Count > 0)
                _tableCollection.RemoveAt(0);
           
            foreach (var item in e.Tables)
            {
                _tableCollection.Add(item);
            }
        }
        class Program
        {
            static String ChannelName = null;
            /// <summary>
            /// 
            /// </summary>
            /// <param name="args"></param>
            /// <returns>the process id the hook was installed into</returns>
            public static int InstallHook(ILog log,string processName)
            {
                Int32 TargetPID = 0;
                
                
                try
                {
                    bool targetProcessNotRunning = true;
                    while (targetProcessNotRunning)
                    {
                        var processes = Process.GetProcessesByName(processName);
                        if (processes.Length == 0)
                        {
                            log.Debug(string.Format("'{0}' not found in running processes.  Will keep trying to find it.", processName));
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            TargetPID = processes[0].Id;
                            targetProcessNotRunning = false;
                        }
                    }
                    
                    try
                    {
                        Config.Register(
                            "A shit like demo application.",
                            "Walter.exe",
                            "Injectable.PokerStars.WriteFileHook.dll",
                            "Bot.Core.dll");
                    }
                    catch (ApplicationException appEx)
                    {
                        string msg = "This is an administrative task!  If you are running Windows 7 or Vista, right-click and Run As Administrator.";
                        log.Error(msg, appEx);
                        System.Windows.MessageBox.Show(msg, "Permission denied...");

                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }

                    RemoteHooking.IpcCreateServer<HookHandler>(ref ChannelName, WellKnownObjectMode.SingleCall);

                    RemoteHooking.Inject(
                        TargetPID,
                        "Injectable.PokerStars.WriteFileHook.dll",
                        "Injectable.PokerStars.WriteFileHook.dll",
                        ChannelName);

                }
                catch (Exception ExtInfo)
                {
                    log.Debug(string.Format("There was an error while connecting to target:\r\n{0}", ExtInfo.ToString()));
                }
                return TargetPID;
            }
            
        }
        object _buttonCaption = "Start";
        public object ButtonCaption 
        {
            get
            {
                return _buttonCaption;
            }
            set
            {
                _buttonCaption = value;
                OnPropertyChanged("ButtonCaption");
                
            }
        }
        bool _globalRun = false;
        bool GlobalRun
        {
            get
            {
                return _globalRun;
            }
            set
            {
                _globalRun = value;

                HookHandler.Running = value;

                if (_globalRun)
                {
                    ButtonCaption = "Stop";
                    MyBackground = Brushes.Green;
                }
                else
                {
                    ButtonCaption = "Start";
                    MyBackground = Brushes.Red;
                }
                

                _globalRun = value;
            }
        }
        Brush _myBackground = Brushes.Pink;
        public Brush MyBackground
        {
            get
            {
                return _myBackground;
            }
            set
            {
                _myBackground = value;
                OnPropertyChanged("MyBackground");
            }
        }
        bool _hookInstalled = false;
        
        bool _initialized = false;
        public void Start()
        {
            if (!_initialized)
            {
                Initialize();
            }
            UltimatePokerActionProvider.Instance.Start();
            GlobalRun = true;
        }
        public void Stop()
        {
            UltimatePokerActionProvider.Instance.Stop();
            TableManager.Instance.ClearAllPendingActions();
            GlobalRun = false;
        }
        private void startstopButton_Click(object sender, RoutedEventArgs e)
        {
            if (GlobalRun == false)
                Start();
            else
                Stop();
        }

        private void Initialize()
        {
            //get the process number
            int pid = -1;
            if (!int.TryParse(processName.Text.Trim(), out pid))
            {
                string msg = string.Concat(processName.Text, " is not a number.  Enter the process id of the poker game process");
                _log.Error(msg);
                throw new InvalidOperationException(msg);
            }
            if (!_worker.IsBusy)
                _worker.RunWorkerAsync(pid);

            _initialized = true;
        }

        private void CalibrateScreen_Click(object sender, RoutedEventArgs e)
        {
            //get selected item in listview
            var item = listView.SelectedItem as TableController;
            if (item == null) return;
            SystemWindow w = new SystemWindow(item.WindowHandle);
            Screenshot s = new Screenshot(w);
             s.ShowDialog();
           
        }

        private void StartStop_Click(object sender, RoutedEventArgs e)
        {
            var item = listView.SelectedItem as TableController;
            if (item == null) return;
            if (item.BotIsRunning)
            {
                item.Stop();
            }
            else
            {
                item.Start();
            }
        }

        private void _contextMenu_Opened(object sender, RoutedEventArgs e)
        {
            var item = listView.SelectedItem as TableController;
            if (item == null) return;
            if (item.BotIsRunning)
            {
                StartStop.Header = "Stop";
            }
            else
            {
                StartStop.Header = "Start";
            }
        }

        private void ConfigureStartingHands_Click(object sender, RoutedEventArgs e)
        {
            var item = listView.SelectedItem as TableController;
            if (item == null) return;
            var s = new HandGroupSelector(item.GetPlayableHands(),false);
            //var s = new HandSelector(item.GetPlayableHands());
            
            s.ShowDialog();
            
        }
        private void ConfigureHandPreferences_Click(object sender, RoutedEventArgs e)
        {
            var item = listView.SelectedItem as TableController;
            if (item == null) return;
            var s = new HandConfigurator(item.GetPlayableHands().HandPreferences, false);
            //var s = new HandSelector(item.GetPlayableHands());

            s.ShowDialog();

        }
        private void Window_Closed(object sender, EventArgs e)
        {
            //save process name to config
            var pName = processName.Text.Trim();
            if (string.Compare(Properties.Settings.Default.ProcessName, pName, true) == 0)
                return;

            Properties.Settings.Default.ProcessName = processName.Text;
            Properties.Settings.Default.Save();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //get the process name from config and put it into text box
            processName.Text = Properties.Settings.Default.ProcessName;
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                startstopButton_Click(this, new RoutedEventArgs());
            }
        }

        private void ConfigureHandGroup_Click(object sender, RoutedEventArgs e)
        {
            var fileName = System.IO.Path.Combine(SerializationMethods.ConfigFilesfolderName, SerializationMethods.DefaultStartingHandFile);

            var handsConfig = SerializationMethods.Deserialize<Bot.Core.Objects.HandCollection>(fileName);
            var s = new HandGroupSelector(handsConfig, true);
            s.ShowDialog();
        }

        private void ConfigurePlayingStyle_Click(object sender, RoutedEventArgs e)
        {
            var fileName = System.IO.Path.Combine(SerializationMethods.ConfigFilesfolderName, SerializationMethods.DefaultHandPreferencesFile);
            var prefs = SerializationMethods.Deserialize<HandGroupPreference>(fileName) ?? new HandGroupPreference();
            var s = new HandConfigurator(prefs, true);
            s.ShowDialog();
        }

        private void InterceptedData_Click(object sender, RoutedEventArgs e)
        {
            new InterceptedDataViewer().Show();
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
    }
}
