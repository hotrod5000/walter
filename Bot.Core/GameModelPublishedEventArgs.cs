﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Objects;

namespace Bot.Core
{
    public class GameModelPublishedEventArgs : EventArgs
    {
        public IGameModel Model { get; set; }
    }
}
