﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core
{
    public class HandDealtEventArgs : EventArgs
    {
        public string Card1;
        public string Card2;
        public IntPtr WindowHandle;
    }
}
