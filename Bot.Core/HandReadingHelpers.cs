﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;

namespace Bot.Core
{
    public class HandReadingHelpers
    {
        public static bool IsSuited(List<IPokerCard> hand)
        {
            if(hand.Count < 0)
                return false;
            var firstCardSuit = hand[0].Suit;

            if (hand.Any(x => x.Suit != firstCardSuit))
                return false;

            return true;
        }
    }
}
