﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core.Interfaces
{
    public interface IGlobalRunState
    {
        void Start();
        void Stop();
    }
}
