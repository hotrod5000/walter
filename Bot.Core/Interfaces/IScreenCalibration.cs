﻿using System;
using ManagedWinapi.Windows;
namespace Bot.Core.Interfaces
{
    public interface IScreenCalibration
    {
        POINT CheckButtonWindowCoordinates { get; set; }
        POINT FoldCheckBoxWindowCoordinates { get; set; }
        POINT FoldButtonWindowCoordinates { get; set; }
        POINT RaiseButtonWindowCoordinates { get; set; }
        POINT SliderLeftPositionWindowCoordinates { get; set; }
        POINT ChatWindowCoordinates { get; set; }

    }
}
