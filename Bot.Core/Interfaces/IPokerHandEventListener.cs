﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Objects;

namespace Bot.Core.Interfaces
{
    public interface IPokerHandEventListener
    {
        void Reset();
        void Stop();
        void Start();
        bool IsRunning { get; }
        void CardDealt(IPokerCard card);
        void RequestAction(IEnumerable<PokerAction> actionChoices);
        int PlayerCount { get; set; }
    }
}
