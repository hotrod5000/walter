﻿using System;
using Bot.Core.Enumerations;
namespace Bot.Core.Interfaces
{
    public interface IPlayableHands
    {
        bool Get(int rank1, int rank2, bool suited);
        bool Get(int rank1, int rank2, bool suited, int pos, int numberOfPlayers);
        int GetGroupNumber(int rank1, int rank2, bool suited);
        void Set(int rank1, int rank2, bool suited, int handGroupNumber);
    }
}
