﻿using System;
using Bot.Core.Enumerations;
using ManagedWinapi.Windows;
namespace Bot.Core.Interfaces
{
    public interface IScreenCoordinates
    {
        POINT GetScreenCoordinates(ScreenItems itemType);
        void SetScreenCoordinates(ScreenItems itemType, POINT p);
       
    }
}
