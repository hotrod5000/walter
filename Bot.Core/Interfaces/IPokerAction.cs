﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Enumerations;

namespace Bot.Core.Interfaces
{
    public interface IPokerAction
    {
        void Execute(PokerActionType type);
        void SetWindow(IntPtr windowHandle);
    }
}
