﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core.Interfaces
{
    public delegate void GameModelPublishedEventHandler(object sender, GameModelPublishedEventArgs e);
    public interface IGameModelPublisher
    {
        event GameModelPublishedEventHandler GameModelPublished;
    }
}
