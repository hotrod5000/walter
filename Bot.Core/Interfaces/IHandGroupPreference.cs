﻿using System;
using Bot.Core.Objects;
namespace Bot.Core.Interfaces
{
    public interface IHandGroupPreference
    {
        

        int Get(int numberOfPlayers, int position);
        void Set(int numberOfPlayers, int handGroupNumber,int position);
        HandGroupNumber this[int numberOfPlayers, int position] { get; set; }
    }
}
