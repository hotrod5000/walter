﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Enumerations;

namespace Bot.Core.Interfaces
{
    public interface IPokerCard
    {
        int Rank { get; set; }
        Suits Suit { get; set; }
    }
}
