﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core
{
    public class TableCollectionModifiedEventArgs : EventArgs
    {
        IEnumerable<TableController> _tables;
        public TableCollectionModifiedEventArgs(IEnumerable<TableController> tables)
        {
            _tables = tables;
        }
        public IEnumerable<TableController> Tables { get { return _tables; } }
    }
}
