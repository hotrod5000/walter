﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Enumerations;

namespace Bot.Core
{
    // A delegate type for hooking up change notifications.
    public delegate void HandDealtEventHandler(object sender, HandDealtEventArgs e);

    public class PokerAction
    {
        public PokerAction(PokerActionType actionType, double amount)
        {
            ActionType = actionType;
            Amount = amount;
        }
        public PokerActionType ActionType { get; set; }
        public double Amount { get; set; }
    }
    public class HoldemHand
    {
        List<string> _cards = new List<string>();
        public IntPtr TableWindowHandle { get; set; }
        public void AddCard(string card)
        {
            _cards.Add(card);
            if (_cards.Count == 2)
            {
                OnHandDealt(new HandDealtEventArgs() { Card1 = _cards[0], Card2 = _cards[1], WindowHandle = TableWindowHandle });
            }
        }

        public event HandDealtEventHandler HandDealt;
        // Invoke the Changed event; called whenever list changes
        protected virtual void OnHandDealt(HandDealtEventArgs e)
        {
            if (HandDealt != null)
                HandDealt(this, e);
        }


    }
}
