﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Objects;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using log4net;
using System.Drawing;
using Bot.Core.Interfaces;

namespace Bot.Core
{
    public class SerializationMethods
    {
        public const string ConfigFilesfolderName = "Config files";
        static ILog _log = LogManager.GetLogger(typeof(SerializationMethods));
        const string DefaultScreenCalibrationFile = "DefaultScreenCalibration.cfg";
        public const string DefaultStartingHandFile = "DefaultStartingHandFile.cfg";
        public const string DefaultHandPreferencesFile = "DefaultHandPreferences.cfg";
        public static void SerializeStartingHands(HandCollection c, string fullFilePathToSerializeTo)
        {

            Stream stream = File.Open(fullFilePathToSerializeTo, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, c);
            stream.Close();
        }
        public static void Serialize<T>(T objectToSerialize, string fullFilePathToSerializeTo)
        {

            Stream stream = File.Open(fullFilePathToSerializeTo, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, objectToSerialize);
            stream.Close();
        }
        public static T Deserialize<T>(string fullFilePathToDeSerializeFrom) where T : class
        {
            Stream stream = null;
            try
            {
                object objectToDeSerialize;
                stream = File.Open(fullFilePathToDeSerializeFrom, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToDeSerialize = bFormatter.Deserialize(stream);
                return objectToDeSerialize as T;
            }
            catch (Exception e)
            {
                _log.Error(string.Format("Error deserializing {0}",typeof(T).ToString()), e);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return default(T);
        }
        public static HandCollection DeserializeStartingHands(string fullFilePathToDeSerializeFrom)
        {

            Stream stream = null;
            try
            {
                HandCollection objectToDeSerialize;
                stream = File.Open(fullFilePathToDeSerializeFrom, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToDeSerialize = (HandCollection)bFormatter.Deserialize(stream);
                return objectToDeSerialize;
            }
            catch (Exception e)
            {
                _log.Error(string.Format("Error deserializing starting hands", e));
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return new HandCollection(new HandGroupPreference());
        }
        public static void SerializeScreenCalibration(ScreenCalibration c)
        {
            var fileName = System.IO.Path.Combine(ConfigFilesfolderName, DefaultScreenCalibrationFile);

            Stream stream = File.Open(fileName, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, c);
            stream.Close();
        }
        public static ScreenCalibration DeserializeScreenCalibration()
        {
            var fileName = System.IO.Path.Combine(ConfigFilesfolderName, DefaultScreenCalibrationFile);
            Stream stream = null;
            try
            {
                ScreenCalibration objectToDeSerialize;
                stream = File.Open(fileName, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToDeSerialize = (ScreenCalibration)bFormatter.Deserialize(stream);
                return objectToDeSerialize;
            }
            catch (Exception e)
            {
                _log.Error("Error deserializing screen calibration", e);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return new ScreenCalibration(new Size());
        }
    }
}
