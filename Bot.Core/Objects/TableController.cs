﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using System.ComponentModel;
using Bot.Core.Enumerations;
using ManagedWinapi.Windows;
using Bot.Core.Objects;

namespace Bot.Core
{
    public class TableController : INotifyPropertyChanged, IPokerHandEventListener, IScreenCoordinates
    {
        IntPtr _windowHandle;
        int _playerCount = -1;

        public int PlayerCount
        {
            get { return _playerCount; }
            set {
                _playerCount = value;
                this.OnPropertyChanged("PlayerCount");
                _pokerHandEventListener.PlayerCount = _playerCount;
            }
        }
        public void SitAtTable(int seatNumber)
        {
            PlayerCount = seatNumber;
        }
        public IntPtr WindowHandle
        {
            get { return _windowHandle; }
        }
        
        IPokerHandEventListener _pokerHandEventListener;
        IScreenCoordinates _coords;
        IPokerAction _actionHandler;
        SystemWindow _systemWindow;
        HandCollection _hands;
        public TableController(IntPtr tableHandle, IPokerHandEventListener bot, IPokerAction actionHandler, IScreenCoordinates coords, HandCollection hands)
        {
            _actionHandler = actionHandler;
            _coords = coords;
            _windowHandle = tableHandle;
            _pokerHandEventListener = bot;
            _systemWindow = new SystemWindow(tableHandle);
            _hands = hands;
        }
        public void TableChange(IntPtr tableHandle)
        {
            _windowHandle = tableHandle;
            _systemWindow = new SystemWindow(tableHandle);
            _actionHandler.SetWindow(tableHandle);
            
        }
        public string TableName
        {
            get
            {
                return _systemWindow.Title == string.Empty ? "NO WINDOW" : _systemWindow.Title;
            }
        }
        public string GetChatWindowContents()
        {
            _actionHandler.Execute(PokerActionType.CopyChatToClipboard);
            return System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.Html);
        }
        void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }
        public bool BotIsRunning
        {
            get
            {
                return _pokerHandEventListener.IsRunning;
            }
            set
            {
                if (value != _pokerHandEventListener.IsRunning)
                {
                    if (value)
                    {
                        _pokerHandEventListener.Start();
                    }
                    else
                    {
                        _pokerHandEventListener.Stop();
                    }
                    this.OnPropertyChanged("BotIsRunning");
                }

            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public void Stop()
        {
            //_pokerHandEventListener.Stop();
            BotIsRunning = false;
        }

        public void Start()
        {
            //_pokerHandEventListener.Start();
            BotIsRunning = true;
        }

        public bool IsRunning
        {
            get { return _pokerHandEventListener.IsRunning; }
        }


        public void CardDealt(IPokerCard card)
        {
            _pokerHandEventListener.CardDealt(card);
        }

        public void RequestAction(IEnumerable<PokerAction> actions)
        {
            _pokerHandEventListener.RequestAction(actions);
        }

        public POINT GetScreenCoordinates(ScreenItems itemType)
        {
            return _coords.GetScreenCoordinates(itemType);
        }

        public void SetScreenCoordinates(ScreenItems itemType, POINT p)
        {
            _coords.SetScreenCoordinates(itemType, p);
        }
        public HandCollection GetPlayableHands()
        {
            return _hands;
        }
        public override string ToString()
        {
            return TableName;
        }



        public void Reset()
        {
            _pokerHandEventListener.Reset();
        }
    }
}
