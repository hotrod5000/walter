﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ManagedWinapi.Windows;
using System.ComponentModel;
using System.Threading;
using log4net;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using HtmlAgilityPack;
using System.IO;
using System.Diagnostics;
using Bot.Core.Enumerations;
using Bot.Core.Interfaces;
namespace Bot.Core.Objects
{
    
    public class UltimatePokerActionProvider
    {
        private static volatile UltimatePokerActionProvider instance;
        private static object syncRoot = new Object();
        private ILog _log = LogManager.GetLogger(typeof(UltimatePokerActionProvider));
        public static UltimatePokerActionProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new UltimatePokerActionProvider();
                    }
                }

                return instance;
            }
        }

        private UltimatePokerActionProvider()
        {

        }

        
        
        Thread _thread;
        Dictionary<IntPtr, Parser> _parsers = new Dictionary<IntPtr, Parser>();
        SortedList<long, int> _handHistory = new SortedList<long, int>();
        
        
        bool _runFlag = true;
        bool _pauseReading = false;
        bool _isInitialized = false;
        private void Initialize()
        {
            _thread = new Thread(_worker_DoWork);
            _thread.SetApartmentState(ApartmentState.STA); //need to do this to read from system.windows.clipboard
            _thread.Start();
            _isInitialized = true;
        }
        public void Start()
        {
            if (!_isInitialized)
            {
                Initialize();
            }
            _pauseReading = false;
        }
        void _worker_DoWork()
        {

            while (_runFlag)
            {
                if (_pauseReading)
                {
                    _log.Warn("Ultimate Poker Action Provider is not running!");
                    Thread.Sleep(2000);
                    continue;
                }
                //System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\temp\" + DateTime.Now.Ticks + ".html", true);
                int count = 0;
                //todo make copy or figure out some way to enumerate over this collection while it might be 
                //changed somewhere else
                foreach (var c in TableManager.Instance.TableControllers.ToList())
                {
                    if (!c.IsRunning) continue;
                    count++;
                    var contents = c.GetChatWindowContents();
                    if (!_parsers.ContainsKey(c.WindowHandle))
                    {
                        _parsers.Add(c.WindowHandle, new Parser(_log) { MyPlayerName = "Walbert" });
                    }
                    //file.WriteLine(contents);
                    var parser = _parsers[c.WindowHandle];
                    parser.UpdateWithLatestHtml(contents);
                    parser.InferPlayerCount(contents);
                    StringBuilder b = new StringBuilder();
                    b.AppendLine(string.Format("Game# {0}", parser.GameNumber));
                    b.AppendLine(string.Format("Position: {0}", parser.Position));
                    b.AppendLine(string.Format("Hand: {0}", parser.GetHand()));
                    _log.Debug(b.ToString());

                    if (parser.HasHand() && !_handHistory.ContainsKey(parser.GameNumber))
                    {

                        c.Reset();
                        c.PlayerCount = parser.PlayerCount;
                        if (parser.Position == TablePosition.SB)
                        {
                            parser.PositionValue = 1;
                        }
                        else if (parser.Position == TablePosition.BB)
                        {
                            parser.PositionValue = 2;
                        }
                        else
                        {
                            //if we were big blind last hand then we'll be the button this hand
                            if (_handHistory.Count>0 && _handHistory.Last().Value == 1)
                                parser.PositionValue = c.PlayerCount;
                            else
                            {
                                parser.PositionValue--;
                                if (parser.PositionValue < 1)
                                {
                                    parser.PositionValue = c.PlayerCount; //assume button until we figure it out
                                }
                            }
                        }
                        _log.Debug("Position value: " + parser.PositionValue.ToString());
                        TableManager.Instance.NewHandReceived(c.WindowHandle, parser.Hand);
                        TableManager.Instance.ReceiveGameModel(parser);
                        _handHistory.Add(parser.GameNumber, parser.PositionValue);
                        c.RequestAction(new List<PokerAction>());
                    }

                }
                

                //file.Close();
                //file.Dispose();
                Thread.Sleep(5000);
            }
            _log.Debug("Ultimate Poker action reader thread stopped.");
        }



        public void Stop()
        {
            _pauseReading = true;
        }
    }
}
