﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;
using HtmlAgilityPack;
using log4net;

namespace Bot.Core.Objects
{
    public class Parser : IGameModel
    {
        public static Suits getSuit(string p)
        {
            if (p.Contains("spade"))
                return Suits.Spades;
            else if (p.Contains("heart"))
                return Suits.Hearts;
            else if (p.Contains("club"))
                return Suits.Clubs;
            else if (p.Contains("diamond"))
                return Suits.Diamonds;
            return Suits.None;


        }
        string _html;
        ILog _log;
        public string MyPlayerName { get; set; }
        public List<IPokerCard> Hand { get; set; }
        public TablePosition Position { get; set; }
        public long GameNumber { get; set; }
        public int PlayerCount { get; set; }
        public Parser(ILog log)
        {
            _log = log;
        }
        public void Reset()
        {
            Position = TablePosition.Unknown;
            Hand = null;
            GameNumber = -1;
        }
        public void InferPlayerCount(string html)
        {
            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(html);

            var desc = doc.DocumentNode.Descendants();
            var last = desc.LastOrDefault(x => x.InnerHtml.Contains("flop is"));
            if (last == null) return ;
            var span = last.ParentNode;
            if (span == null) return;
            Dictionary<string, string> players = new Dictionary<string, string>();
            while (span != null && !span.InnerText.Contains("Game starts"))
            {
                string innerText = span.InnerText.Replace(Environment.NewLine, string.Empty);
                if (innerText.Contains(" folds"))
                {
                    string player = innerText.Split(new string[] { " folds" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    players[player] = "fold";
                }
                else if (innerText.Contains(" calls"))
                {
                    string player = innerText.Split(new string[] { " calls" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    players[player] = "call";
                }
                else if (innerText.Contains(" raises"))
                {
                    string player = innerText.Split(new string[] { " raises" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    players[player] = "raise";
                }
                else if (innerText.Contains(" checks"))
                {
                    string player = innerText.Split(new string[] { " checks" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    players[player] = "check";
                }

                span = span.PreviousSibling;
            }
            if (span != null)
            {
                PlayerCount = players.Count;
                _log.Debug(string.Format("Player count is {0} based on reading the preflop action", PlayerCount));

            }


        }
        public void UpdateWithLatestHtml(string html)
        {
            Reset();
            _html = html;

            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(html);

            var desc = doc.DocumentNode.Descendants();
            var last = desc.LastOrDefault(x => x.InnerHtml.Contains("Game starts"));
            if (last == null) return;
            var span = last.ParentNode;
            if (span == null) return;
            string handNumber = span.InnerText.Split('#')[1];
            GameNumber = long.Parse(handNumber);

            while (span != null)
            {

                string innerText = span.InnerText.Replace(Environment.NewLine, string.Empty);
                if (innerText.Contains("small blind"))
                {

                    string playerName = innerText.Split(new string[] { "posts" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    if (string.Compare(playerName, MyPlayerName) == 0)
                    {
                        Position = TablePosition.SB;
                    }

                }
                else if (span.HasAttributes &&
                     span.Attributes["content"] != null &&
                     span.Attributes["content"].Value.Contains("small blind"))
                {
                    string x = span.Attributes["content"].Value.Replace(Environment.NewLine, string.Empty);
                    string playerName = x.Split(new string[] { "posts" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    if (string.Compare(playerName, MyPlayerName) == 0)
                    {
                        Position = TablePosition.SB;
                    }

                }
                else if (innerText.Contains("big blind"))
                {
                    string playerName = innerText.Split(new string[] { "posts" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    if (string.Compare(playerName, MyPlayerName) == 0)
                    {
                        Position = TablePosition.BB;
                    }

                }
                else if (span.HasAttributes &&
                     span.Attributes["content"] != null &&
                     span.Attributes["content"].Value.Contains("big blind"))
                {
                    string x = span.Attributes["content"].Value.Replace(Environment.NewLine, string.Empty);
                    string playerName = x.Split(new string[] { "posts" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                    if (string.Compare(playerName, MyPlayerName) == 0)
                    {
                        Position = TablePosition.BB;
                    }

                }
                else if (innerText.Contains("hole cards"))
                {
                    var rank1 = getRank(span.ChildNodes[1].InnerText.Substring(span.ChildNodes[1].InnerText.Length - 2, 2).Trim());
                    var suit1 = getSuit(span.ChildNodes[2].Attributes["src"].Value);

                    var rank2 = getRank(span.ChildNodes[3].InnerText.Substring(span.ChildNodes[3].InnerText.Length - 2, 2).Trim());
                    var suit2 = getSuit(span.ChildNodes[4].Attributes["src"].Value);


                    var card1 = new PokerCard() { Rank = rank1, Suit = suit1 };
                    var card2 = new PokerCard() { Rank = rank2, Suit = suit2 };

                    Hand = new List<IPokerCard> { card1, card2 };




                }
                else if (span.HasAttributes &&
                     span.Attributes["content"] != null &&
                     span.Attributes["content"].Value.Contains("hole cards"))
                {
                    string x = span.Attributes["content"].Value.Replace(Environment.NewLine, string.Empty);

                }

                span = span.NextSibling;
            }

        }

        private int getRank(string p)
        {
            int rank;
            if (int.TryParse(p, out rank))
            {
                return rank;
            }
            else
            {
                if (string.Compare(p, "A", true) == 0)
                    return 14;
                else if (string.Compare(p, "K", true) == 0)
                    return 13;
                else if (string.Compare(p, "Q", true) == 0)
                    return 12;
                else if (string.Compare(p, "J", true) == 0)
                    return 11;
            }
            return -1;

        }


        public string GetHand()
        {
            if (Hand == null)
            {
                return "unknown";
            }
            else
            {
                return string.Format("{0} {1}", Hand[0], Hand[1]);
            }

        }
        public bool HasHand()
        {
            if (Hand == null) return false;
            if (Hand.Count != 2) return false;
            return true;
        }

        public int PositionValue
        {
            get;
            set;
        }
        public override string ToString()
        {
            return string.Format("Position value: {0}\nPlayer count: {1}", PositionValue, PlayerCount);
        }
    }
}
