﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Enumerations;

namespace Bot.Core.Objects
{
    public struct GameModel : Bot.Core.Objects.IGameModel
    {
        DateTime _timestamp;

        public DateTime Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }
        int _dealerSeatNumber;
        int _mySeatNumber;

        public void ProcessMessage(string item)
        {
            if (item.Contains("dealerPos"))
            {
                int dealerSeatNumber = -1;
                int.TryParse(item.Substring(item.Length - 1), out dealerSeatNumber);
                DealerSeatNumber = dealerSeatNumber;
            }
            else if (item.Contains("sit"))
            {
                int seatNumber = -1;
                int.TryParse(item.Substring(item.Length - 1), out seatNumber);
                DealHandToPlayerAtSeatNumber(seatNumber);
            }
        }

       
        public int MySeatNumber
        {
            get { return _mySeatNumber; }
            set { _mySeatNumber = value; }
        }
        public int DealerSeatNumber
        {
            get { return _dealerSeatNumber; }
            set { _dealerSeatNumber = value; }
        }
        public void Initialize()
        {
            _list = new List<int>();
        }
        List<int> _list;
        public void DealHandToPlayerAtSeatNumber(int seatNumber)
        {
            _list.Add(seatNumber);
        }
        public int PlayerCount
        {
            get
            {
                return _list.Count;
            }
            set { }
        }
        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            b.AppendLine(string.Format("Game summary:"));
            b.AppendLine();
            b.AppendLine(string.Format("Player count: {0}", PlayerCount));
            //foreach (var seatNumber in _list)
            //{
            //    b.AppendLine(string.Format("\t{0} {1} {2}", DealerSeatNumber == seatNumber ? "d" : " ", seatNumber, MySeatNumber == seatNumber ? "me" : ""));
            //}
            b.AppendLine();
            b.AppendLine(string.Format("{0} {1}", PositionValue, GetMyPosition().ToString()));
            return b.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>a value between 1 (small blind) and PlayerCount (button)</returns>
        
        public int PositionValue
        {
            get
            {
                //start with dealer and count until you get to me
                int dealerIndex = -1;
                int mySeatIndex = -1;
                for (int i = 0; i < _list.Count; i++)
                {
                    if (_list[i] == DealerSeatNumber) dealerIndex = i;
                    if (_list[i] == MySeatNumber) mySeatIndex = i;
                }
                int diff;
                if (dealerIndex <= mySeatIndex)
                    diff = mySeatIndex - dealerIndex;
                else
                    diff = PlayerCount - Math.Abs(mySeatIndex - dealerIndex);

                if (diff == 0) return PlayerCount;
                else return diff;
            }
            set { }

        }
        public TablePosition GetMyPosition()
        {
            int pos = PositionValue;
            if (pos == PlayerCount) return TablePosition.Button;
            switch (pos)
            {
                case 1:
                    return TablePosition.SB;
                case 2:
                    return TablePosition.BB;
                case 3:
                    return TablePosition.UTG;
            }
            if (pos == PlayerCount - 1)
                return TablePosition.Cutoff;

            if (pos == PlayerCount - 2)
                return TablePosition.Hijack;

            return TablePosition.Unknown; ;
        }



        
    }


}
