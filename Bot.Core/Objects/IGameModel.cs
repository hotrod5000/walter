﻿using System;
using Bot.Core.Interfaces;
using System.Collections.Generic;
namespace Bot.Core.Objects
{
    public interface IGameModel
    {
        int PositionValue { get; set; } //1=small blind, 2=big blind ... player count=button
        int PlayerCount { get; set; }
        
    }
}
