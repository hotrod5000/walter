﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Enumerations;

namespace Bot.Core.Objects
{
    [Serializable]
    public class HandGroupPreference : Bot.Core.Interfaces.IHandGroupPreference
    {
        /// <summary>
        /// Dictionary of playerCount to another dictionary of tablePosition to handGroupNumber
        /// </summary>
        Dictionary<int, Dictionary<int, HandGroupNumber>> _absoluteDict = new Dictionary<int, Dictionary<int, HandGroupNumber>>();
        public HandGroupPreference()
        {
            Initialize();
        }
        private void Initialize()
        {
            for (int playerCount = 2; playerCount < 11; playerCount++)
            {
                _absoluteDict.Add(playerCount, new Dictionary<int, HandGroupNumber>());
                for (int tablePosition = 1; tablePosition <= playerCount; tablePosition++)
                {
                    _absoluteDict[playerCount][tablePosition] = new HandGroupNumber(GetHandGroupNumber(playerCount, tablePosition));
                }
            }
        }

        private int GetHandGroupNumber(int playerCount, int tablePosition)
        {
            switch (playerCount)
            {
                case 2:
                case 3:
                    return 8;
                case 4:
                    if (tablePosition == 3)
                        return 5;
                    else
                        return 8;
                case 5:
                    if (tablePosition == 3)
                        return 3;
                    if (tablePosition == 4)
                        return 5;
                    else
                        return 8;
                case 6:
                    if (tablePosition == 3)
                        return 3;
                    if (tablePosition == 4)
                        return 4;
                    if (tablePosition == 5)
                        return 5;
                    else
                        return 8;
                case 7:
                case 8:
                case 9:
                case 10:
                default:
                    return tablePosition;



            }

        }
        public int Get(int numberOfPlayers, int position)
        {
            if (!_absoluteDict.ContainsKey(numberOfPlayers)) return 0;
            return _absoluteDict[numberOfPlayers][position].Number;
        }

        public void Set(int numberOfPlayers, int handGroupNumber, int position)
        {
            if (!_absoluteDict.ContainsKey(numberOfPlayers))
                _absoluteDict[numberOfPlayers] = new Dictionary<int, HandGroupNumber>();

            _absoluteDict[numberOfPlayers][position].Number = handGroupNumber;
        }
        public HandGroupNumber this[int numberOfPlayers, int position]
        {
            get
            {
                return _absoluteDict[numberOfPlayers][position];
            }
            set
            {
                _absoluteDict[numberOfPlayers][position] = value;
            }
        }
    }
}
