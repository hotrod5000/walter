﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ManagedWinapi.Windows;
using Size = System.Drawing.Size;
using log4net;
namespace Bot.Core.Objects
{
    [Serializable]
    public class ScreenCalibration : IScreenCalibration, IScreenCoordinates
    {
        [NonSerialized]
        static ILog _log = LogManager.GetLogger(typeof(ScreenCalibration));
        Size _windowSize;

        public Size WindowSize
        {
            get { return _windowSize; }
            set { _windowSize = value; }
        }
        public ScreenCalibration()
        {
        }
        public ScreenCalibration(Size windowSize)
            : this()
        {
            _windowSize = windowSize;
        }
        public POINT FoldCheckBoxWindowCoordinates { get; set; }
        public POINT RaiseButtonWindowCoordinates { get; set; }
        public POINT SliderLeftPositionWindowCoordinates { get; set; }



        public POINT GetScreenCoordinates(Enumerations.ScreenItems itemType)
        {
            switch (itemType)
            {
                case ScreenItems.FoldCheckBox:
                    return FoldCheckBoxWindowCoordinates;
                case ScreenItems.RaiseButton:
                    return RaiseButtonWindowCoordinates;
                case ScreenItems.SliderLeftPosition:
                    return SliderLeftPositionWindowCoordinates;
            }
            return new POINT();
        }


        public void SetScreenCoordinates(Enumerations.ScreenItems itemType, POINT p)
        {
            switch (itemType)
            {
                case ScreenItems.FoldCheckBox:
                    FoldCheckBoxWindowCoordinates = p;
                    break;
                case ScreenItems.RaiseButton:
                    RaiseButtonWindowCoordinates = p;
                    break;
                case ScreenItems.SliderLeftPosition:
                    SliderLeftPositionWindowCoordinates = p;
                    break;
                case ScreenItems.FoldButton:
                    FoldButtonWindowCoordinates = p;
                    break;
                case ScreenItems.CheckButton:
                    CheckButtonWindowCoordinates = p;
                    break;
                case ScreenItems.ChatWindow:
                    ChatWindowCoordinates = p;
                    break;                    


            }
        }





        public POINT CheckButtonWindowCoordinates { get; set; }


        public POINT FoldButtonWindowCoordinates { get; set; }



        public POINT ChatWindowCoordinates{ get; set; }
        
    }
}
