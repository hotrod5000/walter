﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core.Objects
{
    [Serializable]
    public class HandGroupNumber
    {
        public HandGroupNumber(int number)
        {
            Number = number;
        }
        public int Number
        {
            get;
            set;
        }

    }
}
