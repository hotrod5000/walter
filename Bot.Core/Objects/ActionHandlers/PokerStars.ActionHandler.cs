﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using System.Windows.Forms;
using System.Threading;
using Bot.Core.Enumerations;
using ManagedWinapi.Windows;
using log4net;

namespace Bot.Core.Objects.ActionHandlers
{
    public class PokerStarsActionHandler : IPokerAction, IScreenCoordinates
    {
        ILog _log = LogManager.GetLogger(typeof(PokerStarsActionHandler));
        Random _random ;
        IntPtr _tableHandle;
        ScreenCalibration _screenCalibration;

        public PokerStarsActionHandler(IntPtr tableHandle, ScreenCalibration screenCalibration)
        {
            _random = new Random(DateTime.Now.Second);
            _screenCalibration = screenCalibration;
            _tableHandle = tableHandle;
        }
        public void Execute(PokerActionType type)
        {
            //wait somewhere between no time and 1.5 seconds.  idea is to make bot not look too robotic.
            int waitTime = _random.Next(2000);
            _log.Debug(string.Format("Waiting {0} seconds before executing.", waitTime / 1000));
            Thread.Sleep(waitTime);
            switch (type)
            {
                case PokerActionType.Shove:
                    Shove();
                    break;
                case PokerActionType.Fold:
                    Fold();
                    break;
                case PokerActionType.Check:
                    Check();
                    break;
                case PokerActionType.CopyChatToClipboard:
                    CopyChatWindowContentsToClipboard();
                    break;

            }
        }
        void Check()
        {

            //get current cursor position so we can set it back
            System.Drawing.Point currentPos = Control.MousePosition;
            
            POINT p = _screenCalibration.CheckButtonWindowCoordinates;
            //turn window coordinats into screen coordinates
            ImportedFunctionsDeclarations.ClientToScreen(_tableHandle, ref p);

            ImportedFunctionsDeclarations.ShowWindow(_tableHandle, 9);
            ImportedFunctionsDeclarations.SetForegroundWindow(_tableHandle);

            //int xRandom = _random.Next(-8, 8);
            //int yRandom = _random.Next(-5, 5);
            //p.X += xRandom; _log.Debug(string.Format("adding {0} to x click pos", xRandom));
            //p.Y += yRandom; _log.Debug(string.Format("adding {0} to y click pos", yRandom));
            MouseClickAtLocation(p);

            //put mouse back where it was, when running in release mode
#if !DEBUG
            ImportedFunctionsDeclarations.SetCursorPos(currentPos.X, currentPos.Y);
#endif

        }
        void CopyChatWindowContentsToClipboard()
        {

            //get current cursor position so we can set it back
            System.Drawing.Point currentPos = Control.MousePosition;

            POINT p = _screenCalibration.ChatWindowCoordinates;
            //turn window coordinats into screen coordinates
            ImportedFunctionsDeclarations.ClientToScreen(_tableHandle, ref p);

            ImportedFunctionsDeclarations.ShowWindow(_tableHandle, 9);
            ImportedFunctionsDeclarations.SetForegroundWindow(_tableHandle);

            int xRandom = _random.Next(-8, 8);
            int yRandom = _random.Next(-5, 5);
            p.X += xRandom; _log.Debug(string.Format("adding {0} to x click pos", xRandom));
            p.Y += yRandom; _log.Debug(string.Format("adding {0} to y click pos", yRandom));
            MouseClickAtLocation(p);
            Thread.Sleep(1000);
            SendKeys.SendWait("^(a)");
            SendKeys.SendWait("^(c)");

            //put mouse back where it was, when running in release mode
#if !DEBUG
            //ImportedFunctionsDeclarations.SetCursorPos(currentPos.X, currentPos.Y);
#endif

        }
        void Fold()
        {
            //get current cursor position so we can set it back
            System.Drawing.Point currentPos = Control.MousePosition;
            //get current active window so we can set it back
            var foregroundWindow = ManagedWinapi.Windows.SystemWindow.ForegroundWindow;

            //POINT p = GetPointToClickForFoldAction();
            POINT p = GetPointToClickForFoldAction2();
            
            
            MouseClickAtLocation(p);

            //put mouse back where it was, when running in release mode
#if !DEBUG
            ImportedFunctionsDeclarations.SetCursorPos(currentPos.X, currentPos.Y);
#endif
            ManagedWinapi.Windows.SystemWindow.ForegroundWindow = foregroundWindow;
        }
        /// <summary>
        /// idea here is that we want to use the fold button most of the time, but use the fold
        /// check box some of the time, in order to make it look like we're not a bot
        /// </summary>
        /// <returns></returns>
        private POINT GetPointToClickForFoldAction()
        {
            POINT p;
            int xRandom;
            int yRandom;
            //use the button 80% of the time, use the check box 20% of the time
            if (_random.NextDouble() > 0.2)
            {
                p = _screenCalibration.FoldButtonWindowCoordinates;
                xRandom = _random.Next(-10, 10); //add random amount of units to position
                yRandom = _random.Next(-5, 5);
            }
            else
            {
                p = _screenCalibration.FoldCheckBoxWindowCoordinates;
                xRandom = _random.Next(-3, 3); //add random amount of units to position
                yRandom = _random.Next(-3, 3);
            }
            //turn window coordinats into screen coordinates
            ImportedFunctionsDeclarations.ClientToScreen(_tableHandle, ref p);

            ImportedFunctionsDeclarations.ShowWindow(_tableHandle, 9);
            ImportedFunctionsDeclarations.SetForegroundWindow(_tableHandle);

            
            p.X += xRandom; _log.Debug(string.Format("adding {0} to x click pos", xRandom));
            p.Y += yRandom; _log.Debug(string.Format("adding {0} to y click pos", yRandom));

            return p;
        }
        private POINT GetPointToClickForFoldAction2()
        {
            POINT p;
            
            //use the button 80% of the time, use the check box 20% of the time
            
            p = _screenCalibration.FoldCheckBoxWindowCoordinates;
                
            
            //turn window coordinats into screen coordinates
            ImportedFunctionsDeclarations.ClientToScreen(_tableHandle, ref p);

            ImportedFunctionsDeclarations.ShowWindow(_tableHandle, 9);
            ImportedFunctionsDeclarations.SetForegroundWindow(_tableHandle);

            return p;
        }
        private static void MouseClickAtLocation(POINT p)
        {
            ImportedFunctionsDeclarations.SetCursorPos(p.X, p.Y);
            ////Mouse Right Down and Mouse Right Up
            ImportedFunctionsDeclarations.mouse_event((uint)ImportedFunctionsDeclarations.MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
            ImportedFunctionsDeclarations.mouse_event((uint)ImportedFunctionsDeclarations.MouseEventFlags.LEFTUP, 0, 0, 0, 0);
        }
        void Shove()
        {
            //get current cursor position so we can set it back
            System.Drawing.Point currentPos = Control.MousePosition;
            //put the mouse on the slider
            POINT p = _screenCalibration.SliderLeftPositionWindowCoordinates;
            //turn window coordinats into screen coordinates
            ImportedFunctionsDeclarations.ClientToScreen(_tableHandle, ref p);

            ImportedFunctionsDeclarations.ShowWindow(_tableHandle, 9);
            ImportedFunctionsDeclarations.SetForegroundWindow(_tableHandle);
            ImportedFunctionsDeclarations.SetCursorPos(p.X, p.Y);
            //push the mouse button down
            ImportedFunctionsDeclarations.mouse_event((uint)ImportedFunctionsDeclarations.MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
            //move the mouse to the end of the slider
            ImportedFunctionsDeclarations.SetCursorPos(p.X + 10000, p.Y);
            //lift the mouse button
            ImportedFunctionsDeclarations.mouse_event((uint)ImportedFunctionsDeclarations.MouseEventFlags.LEFTUP, 0, 0, 0, 0);
            //push the raise button
            POINT p2 = _screenCalibration.RaiseButtonWindowCoordinates;
            //turn window coordinats into screen coordinates
            ImportedFunctionsDeclarations.ClientToScreen(_tableHandle, ref p2);
            MouseClickAtLocation(p2);
            //put mouse back where it was, when running in release mode
#if !DEBUG
            ImportedFunctionsDeclarations.SetCursorPos(currentPos.X, currentPos.Y);
#endif
        }


        public POINT GetScreenCoordinates(ScreenItems itemType)
        {
            return _screenCalibration.GetScreenCoordinates(itemType);
        }

        public void SetScreenCoordinates(ScreenItems itemType, POINT p)
        {
            _screenCalibration.SetScreenCoordinates(itemType, p);
        }


        public void SetWindow(IntPtr windowHandle)
        {
            _tableHandle = windowHandle;
        }
    }
}
