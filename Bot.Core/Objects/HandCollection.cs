﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;
using log4net;

namespace Bot.Core.Objects
{
    [Serializable]
    public class HandCollection : IPlayableHands
    {
        [NonSerialized]
        static ILog _log = LogManager.GetLogger(typeof(ScreenCalibration));

        IHandGroupPreference _preferences;

        public IHandGroupPreference HandPreferences
        {
            get { return _preferences; }
            set { _preferences = value; }
        }
        public HandCollection(IHandGroupPreference prefs)
        {
            _preferences = prefs;
        }

        int[,] _hands = new int[15,15];
        public void Set(int rank1, int rank2, bool suited, int handGroupNumber)
        {
            int smaller = rank1 < rank2 ? rank1 : rank2;
            int bigger = rank1 > rank2 ? rank1 : rank2;
            //put the smaller number first if not suited
            if(suited)
                _hands[smaller, bigger] = handGroupNumber;
            else
                _hands[bigger, smaller] = handGroupNumber;

        }
        public bool Get(int rank1, int rank2, bool suited)
        {
            return GetGroupNumber(rank1, rank2, suited) > 0;
        }
        public bool Get(int rank1, int rank2, bool suited, int position, int numberOfPlayers)
        {
            var handGroupNumber = GetGroupNumber(rank1, rank2,suited);
            var minimumRequiredGroupNumber = _preferences.Get(numberOfPlayers, position);
            _log.Info(string.Format("my hand: group {0}", handGroupNumber));
            _log.Info(string.Format("required: group {0}", minimumRequiredGroupNumber));
            return handGroupNumber>0 && handGroupNumber <= minimumRequiredGroupNumber;
        }


        public int GetGroupNumber(int rank1, int rank2, bool suited)
        {
            int smaller = rank1 < rank2 ? rank1 : rank2;
            int bigger = rank1 > rank2 ? rank1 : rank2;
            //put the smaller number first if not suited
            if (suited)
                return _hands[smaller, bigger] ;
            else
                return _hands[bigger, smaller] ;
        }
    }
}
