﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;

namespace Bot.Core.Objects
{
    public class PokerCard : IPokerCard
    {
        public override string ToString()
        {
            return string.Format("{0} {1}", Rank, Suit);
        }
        public PokerCard()
        {

        }
        public PokerCard(string card)
        {
            //last character should be suit
            string suit = card.Substring(card.Length - 1);
            string rank = card.Remove(card.Length - 1);
            Rank = int.Parse(rank);
            switch (suit)
            {
                case "c":
                    Suit = Suits.Clubs;
                    break;
                case "h":
                    Suit = Suits.Hearts;
                    break;
                case "d":
                    Suit = Suits.Diamonds;
                    break;
                case "s":
                    Suit = Suits.Spades;
                    break;
            }
        }
        public int Rank
        {
            get;
            set;

        }

        public Suits Suit
        {
            get;
            set;
        }
    }
}
