﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Objects.Bots;
using Bot.Core.Objects.ActionHandlers;
using Bot.Core.Enumerations;
using ManagedWinapi.Windows;
using System.Diagnostics;
using POINT = Bot.Core.POINT;
using System.IO;
using log4net;
namespace Bot.Core.Objects
{
    public class TableManager : IGameModelPublisher
    {
        private static volatile TableManager instance;
        private static object syncRoot = new Object();
        private ILog _log = LogManager.GetLogger(typeof(TableManager));
        public IGlobalRunState GlobalRunState { get; set; }
        public static TableManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TableManager();
                    }
                }

                return instance;
            }
        }

        private TableManager()
        {

        }
        public event EventHandler<TableCollectionModifiedEventArgs> TableAdded;
        // Invoke the Changed event; called whenever list changes:
        protected virtual void OnTableAdded(TableCollectionModifiedEventArgs e)
        {
            if (TableAdded != null)
                TableAdded(this, e);
        }


        Dictionary<IntPtr, TableController> _tableControllers = new Dictionary<IntPtr, TableController>();

        public IEnumerable<TableController> TableControllers
        {
            get
            {
                return _tableControllers.Values;
            }
        }
        public bool HasController(IntPtr table)
        {
            return _tableControllers.ContainsKey(table);
        }
        public TableController GetController(IntPtr table)
        {
            if (HasController(table))
                return _tableControllers[table];
            return null;
        }
        public void Synchronize(IEnumerable<SystemWindow> windows)
        {

            
            bool changed = false;
            var itemsToRemove = new List<IntPtr>();
            //find all closed tables
            foreach (var item in _tableControllers)
            {
                var existing = windows.FirstOrDefault(x => x.HWnd == item.Key);
                if (existing == null)
                    itemsToRemove.Add(item.Key);
            }
            //add new tables
            foreach (var item in windows)
            {
                var tableChange = false;
                if (!_tableControllers.ContainsKey(item.HWnd))
                {
                    //transfer an existing tournament bot to its new table
                    if (item.Title.Contains("Tournament"))
                    {
                        IntPtr existingTournamentTable = new IntPtr(0);
                        long tourneyNumber = 0;
                        //split on spaces
                        var chunks = item.Title.Split(' ');
                        foreach (var chunk in chunks)
                        {
                            try
                            {
                                tourneyNumber = long.Parse(chunk);
                                _log.Debug(string.Format("Parsed '{0}' from window title...assuming that's the tourney number", tourneyNumber));
                                existingTournamentTable = item.HWnd;
                                break;
                            }
                            catch
                            { }
                        }
                        if (tourneyNumber != 0) //it's a tournament
                        {
                            TableController existingBot = _tableControllers.Values.FirstOrDefault(x => x.TableName.Contains(tourneyNumber.ToString()));
                            if (existingBot != null)
                            {
                                tableChange = true;
                                _tableControllers[item.HWnd] = existingBot;
                                existingBot.TableChange(item.HWnd);
                                _log.Debug(string.Format("Tournament table change in tourney #{0}", tourneyNumber));

                            }
                        }
                    }
                    if (!tableChange)
                    {
                        _tableControllers[item.HWnd] = CreateTableController(item.HWnd);
                    }
                    changed = true;
                }
            }
#if !DEBUG
            foreach (var item in itemsToRemove)
            {
                _tableControllers.Remove(item);
                changed = true;
            }
#endif            
            if (changed)
                OnTableAdded(new TableCollectionModifiedEventArgs(_tableControllers.Values));

        }
        public void SitAtTable(IntPtr tableHandle, int seatNumber)
        {
            TableController c;
            if (!_tableControllers.ContainsKey(tableHandle))
            {
                var newController = CreateTableController(tableHandle);
                _tableControllers[tableHandle] = newController;
            }
            if (_tableControllers.TryGetValue(tableHandle, out c))
            {
                c.SitAtTable(seatNumber);
                _log.Debug(string.Format("Taking a seat at table {0} in seat #{1}", c.TableName, seatNumber));
            }
        }
        TableController CreateTableController(IntPtr windowHandle)
        {
            var fileName = System.IO.Path.Combine(SerializationMethods.ConfigFilesfolderName, SerializationMethods.DefaultStartingHandFile);
            var startingHands = SerializationMethods.DeserializeStartingHands(fileName);
            var screenCalibration = GetDefaultScreenCalibration();
            var actionHandler = new PokerStarsActionHandler(windowHandle, screenCalibration);
            IPokerHandEventListener bot = new FoldOrNotifyBot(actionHandler, startingHands, this) { GlobalRunState = GlobalRunState };
            return new TableController(windowHandle, bot, actionHandler, actionHandler, startingHands);

        }
        ScreenCalibration GetDefaultScreenCalibration()
        {
            return SerializationMethods.DeserializeScreenCalibration();
        }
       
        public void NewHandReceived(IntPtr tableHandle, IEnumerable<IPokerCard> cards)
        {
            if (!_tableControllers.ContainsKey(tableHandle))
            {
                Debug.Fail("shouldn't be here.  we received a hand from a table that is not in our table list");
                _tableControllers[tableHandle] = CreateTableController(tableHandle);
            }
            var controller = _tableControllers[tableHandle];
            
            foreach (var card in cards)
            {
                controller.CardDealt(card);
            }
            OnTableAdded(new TableCollectionModifiedEventArgs(_tableControllers.Values));

        }
        public void RequestAction(IntPtr tableHandle,IEnumerable<PokerAction> actionChoices)
        {
            TableController controller;
            if (_tableControllers.TryGetValue(tableHandle, out controller))
            {
                controller.RequestAction(actionChoices);
            }
        }

        public void ClearAllPendingActions()
        {
            foreach (var controller in _tableControllers.Values)
            {
                controller.Reset();
            }
        }
        public void Reset(IntPtr table)
        {
            TableController controller;
            if (_tableControllers.TryGetValue(table, out controller))
            {
                controller.Reset();
            }
        }
        protected virtual void OnGameModelReceived(GameModelPublishedEventArgs e)
        {
            if (GameModelPublished != null)
                GameModelPublished(this, e);
        }
        public void ReceiveGameModel(IGameModel model)
        {
            OnGameModelReceived(new GameModelPublishedEventArgs() { Model = model });

        }
        public event GameModelPublishedEventHandler GameModelPublished;
        
    }
}
