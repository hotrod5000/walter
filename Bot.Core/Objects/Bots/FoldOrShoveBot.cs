﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Objects.ActionHandlers;

namespace Bot.Core.Objects.Bots
{
    public class FoldOrShoveBot : BaseBot
    {
        public FoldOrShoveBot(IPokerAction actionHandler, IPlayableHands hands)
        {
            _actionHandler = actionHandler;
            _startingHands = hands;
        }

        protected override void ExecutePreFlop()
        {
            IPokerCard c1 = _cards[0];
            IPokerCard c2 = _cards[1];

            if (IsPlayableStartingHand(c1, c2))
                Shove();
            else
                Fold();
        }
    }
}
