﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;
using log4net;

namespace Bot.Core.Objects.Bots
{
    public class BaseBot : IPokerHandEventListener
    {
        DateTime _lastHandReceivedTimestamp;
        protected IGameModelPublisher _gameModelPublisher;
        protected IPlayableHands _startingHands;
        protected ILog _log = LogManager.GetLogger(typeof(BaseBot));
        protected bool _isRunning = true;
        protected List<IPokerCard> _cards = new List<IPokerCard>();
        protected IPokerAction _actionHandler;
        public IGlobalRunState GlobalRunState { get; set; }
        protected void _gameModelPublisher_GameModelPublished(object sender, GameModelPublishedEventArgs e)
        {
            _game = e.Model;
            _gameModelReceived = true;
            //game model received.  it might not be one that we're interested in
            if (_gameModelReceived && _handReceived)
            {
                ExecutePreFlop();
            }


        }
        protected bool IsPlayableStartingHand(IPokerCard c1, IPokerCard c2)
        {
            return _startingHands.Get(c1.Rank, c2.Rank, c1.Suit == c2.Suit, _game.PositionValue, PlayerCount);
        }

        public void CardDealt(IPokerCard card)
        {
            _cards.Add(card);
            HandleNewCard();
        }

        void HandleNewCard()
        {
            if (_cards.Count == 2)
            {
                _log.Info("Hand dealt");
                foreach (var card in _cards)
                    _log.Info(string.Format("\t{0}", card));


                _lastHandReceivedTimestamp = DateTime.Now;
                _handReceived = true;

                if (_handReceived && _gameModelReceived)
                    ExecutePreFlop();
            }
        }
        protected virtual void ExecutePreFlop()
        {
            _log.Info(_game.ToString());

        }
        protected IGameModel _game;

        protected void Fold()
        {
            _log.Info("setting pending action to 'Fold'");
            _pendingAction = PokerActionType.Fold;

        }
        protected void Check()
        {
            _log.Info("setting pending action to 'Check'");
            _pendingAction = PokerActionType.Check;

        }

        protected void Shove()
        {
            _log.Info("setting pending action to 'Shove'");
            _pendingAction = PokerActionType.Shove;
        }

        protected PokerActionType _pendingAction;
        protected bool _handReceived;
        private bool _gameModelReceived;
        public void RequestAction(IEnumerable<PokerAction> actionChoices)
        {
            if (!_isRunning)
            {
                _log.Debug(string.Format("NOT executing pending action {0}.  Bot is not running.", _pendingAction.ToString()));
                return;
            }
            //if we have the chance to check, and we were planning on folding, then check and 
            //special notify
            if (actionChoices.Any(x => x.ActionType == PokerActionType.Check) && _pendingAction == PokerActionType.Fold)
            {
                _log.Debug("have the option to check along in big blind, so fuck yea");
                Console.Beep();
                _actionHandler.Execute(PokerActionType.Check);
                _pendingAction = PokerActionType.None;
                return;
            }

            _log.Debug(string.Format("executing pending action {0}", _pendingAction.ToString()));
            if (_isRunning)
                _actionHandler.Execute(_pendingAction);
            _pendingAction = PokerActionType.None;
        }

        public void Stop()
        {
            _log.Debug("bot stopped... setting pending action to 'none'");
            _pendingAction = PokerActionType.None;
            _isRunning = false;
        }

        public void Start()
        {
            _log.Debug("bot started...");
            _isRunning = true;
        }




        public bool IsRunning
        {
            get { return _isRunning; }
        }

        public void Reset()
        {
            _pendingAction = PokerActionType.None;
            _cards.Clear();
            _game = null;
            _gameModelReceived = false;
            _handReceived = false;
            _log.Debug("Reset ---------------------------------------------->");
        }


        public int PlayerCount
        {
            get;
            set;
        }
    }
}
