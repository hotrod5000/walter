﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;
using System.Media;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Bot.Core.Enumerations;

namespace Bot.Core.Objects.Bots
{
    public class FoldOrNotifyBot : BaseBot
    {
        
        public FoldOrNotifyBot(IPokerAction actionHandler, IPlayableHands startingHands, IGameModelPublisher gameModelPublisher)
        {
            _gameModelPublisher = gameModelPublisher;
            _gameModelPublisher.GameModelPublished += new GameModelPublishedEventHandler(_gameModelPublisher_GameModelPublished);
            _actionHandler = actionHandler;
            _startingHands = startingHands;
        }

        

        protected override void ExecutePreFlop()
        {
            base.ExecutePreFlop();
            IPokerCard c1 = _cards[0];
            IPokerCard c2 = _cards[1];

            if (IsPlayableStartingHand(c1, c2))
            {
                //stop the ultimate poker copy chat contents thread, cuz it's fucking annoying
                UltimatePokerActionProvider.Instance.Stop();
                Notify(); //notify immediately
                return;
            }
            if (IAmInBigBlind())
            {
                //fold if there is a raise, otherwise, check and notify
                _log.Info("I am in the big blind, i should check if I can rather than fold");
                Check();
                return;
                
            }
            Fold();
        }

        private bool IAmInBigBlind()
        {
            if (_game.PositionValue == 2)
                return true;
            return false;
        }

        private void Notify()
        {
            if (!_isRunning)
            {
                return;
            }
            //string fileName = @"C:\Program Files (x86)\Winamp.exe ";
            //string arguments = Path.Combine(Assembly.GetExecutingAssembly().Location, "boing4.mp3");
            //var fullCommand = string.Concat(fileName, arguments);
            //Process.Start(fullCommand);
            //try
            //{
            //    WaveStream mp3Reader = new Mp3FileReader(@"Sound Files\boing4.mp3");
            //    WaveStream pcmStream = WaveFormatConversionStream.CreatePcmStream(mp3Reader);
            //    WaveStream blockAlignedStream = new BlockAlignReductionStream(pcmStream);
            //}
            //catch(Exception e)
            //{
            //    _log.Error("Notify failed to play sound file",e);
            //}

            _pendingAction = PokerActionType.None;
            if (GlobalRunState != null)
            {
                GlobalRunState.Stop();
            }
            for (int freq = 200; freq < 400; freq += 50)
            {
                Console.Beep(freq, 200);
            }
            //System.Diagnostics.Process.Start(@"sound files\Boing4.mp3");

        }
    }
}
