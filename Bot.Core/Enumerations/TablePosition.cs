﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core.Enumerations
{
    public enum TablePosition
    {
        Unknown,
        SB,
        BB,
        UTG,
        UTGPlusOne,
        UTGPlusTwo,
        Middle,
        Hijack,
        Cutoff,
        Button

        
    }
}
