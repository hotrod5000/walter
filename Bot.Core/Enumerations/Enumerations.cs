﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core.Enumerations
{
    public enum Suits
    {
        None,
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }
}
