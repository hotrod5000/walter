﻿using System;
using System.Collections.Generic;
using System.Text;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;

namespace Bot.Core.Enumerations
{
    
    public enum PokerActionType
    {
        None,
        Check,
        Fold,
        Call,
        Raise,
        Shove,
        Notify,
        CopyChatToClipboard
    }
    
}
