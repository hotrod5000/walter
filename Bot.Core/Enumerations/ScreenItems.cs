﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bot.Core.Enumerations
{
    public enum ScreenItems
    {
        FoldCheckBox,
        FoldButton,
        CheckButton,
        SliderLeftPosition,
        RaiseButton,
        ChatWindow
        
    }
}
