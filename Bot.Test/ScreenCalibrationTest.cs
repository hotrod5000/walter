﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bot.Core.Objects;
using System.IO;
using ManagedWinapi.Windows;
using System.Runtime.Serialization.Formatters.Binary;
using Size = System.Drawing.Size;

namespace Bot.Test
{
    /// <summary>
    /// Summary description for ScreenCalibrationTest
    /// </summary>
    [TestClass]
    public class ScreenCalibrationTest
    {
        public ScreenCalibrationTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CanSerializeScreenCalibration()
        {
            ScreenCalibration sut = new ScreenCalibration(new Size(111,222));
            sut.FoldCheckBoxWindowCoordinates = new Bot.Core.POINT(50, 60);
            sut.RaiseButtonWindowCoordinates = new Core.POINT(100, 200);
            sut.ChatWindowCoordinates = new Core.POINT(222, 333);
            //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(ScreenCalibration));
            //TextWriter tw = new StreamWriter(@"c:\hand.txt");
            //x.Serialize(tw, sut);
            //tw.Close();
            
            Stream stream = File.Open(@"c:\hand.txt", FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, sut);
            stream.Close();

            ScreenCalibration objectToDeSerialize;
            Stream stream2 = File.Open(@"c:\hand.txt", FileMode.Open);
            BinaryFormatter bFormatter2 = new BinaryFormatter();
            objectToDeSerialize = (ScreenCalibration)bFormatter.Deserialize(stream2);
            stream2.Close();
            Assert.AreEqual(50, objectToDeSerialize.FoldCheckBoxWindowCoordinates.X);
            Assert.AreEqual(60, objectToDeSerialize.FoldCheckBoxWindowCoordinates.Y);
            Assert.AreEqual(100, objectToDeSerialize.RaiseButtonWindowCoordinates.X);
            Assert.AreEqual(111, sut.WindowSize.Width);




        }
    }
}
