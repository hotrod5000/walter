﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bot.Core.Objects;
using Bot.Core;

namespace Bot.Test
{
    [TestClass]
    public class HandGroupPreferenceTest
    {
        [TestMethod]
        public void CanSerializeHandGroupPreference()
        {
            string filename = "test.cfg";
            var sut = new HandGroupPreference();
            sut.Set(10, 1, 3);
            SerializationMethods.Serialize<HandGroupPreference>(sut, filename);

            var x = SerializationMethods.Deserialize<HandGroupPreference>(filename);
            Assert.AreEqual(1,sut.Get(10, 3));
        }
    }
}
