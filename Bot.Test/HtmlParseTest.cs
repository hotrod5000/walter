﻿using Bot.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using HtmlAgilityPack;
using System.Diagnostics;
using Bot.Core.Enumerations;
using Bot.Core.Objects;
namespace Bot.Test
{


    /// <summary>
    ///This is a test class for HoldemHandTest and is intended
    ///to contain all HoldemHandTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HtmlParseTest
    {

        #region MyRegion
        string _html2 = @"
Version:1.0
StartHTML:000000234
EndHTML:000014684
StartFragment:000000961
EndFragment:000014646
StartSelection:000000961
EndSelection:000014646
SourceURL:file://C:\Documents and Settings\Rodney\Local Settings\Temp\ggp2F10.htm
<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">

<HTML><HEAD>
<SCRIPT language=Javascript>
document.write('<BASE href=""');
document.write(window.external.imagesPath);
document.write('"">');
</SCRIPT>
<BASE 
href=""C:/Documents and Settings/All Users/Application Data/Ultimate Gaming/Ultimate Poker/images/"">
<STYLE type=text/css>
body { background-color: #000000; margin: 1px; word-wrap: break-word; }
span { color: #ffffff; font-family: Arial; font-size: 11px; }
span.player { }
span.observer { color: #ff0000; font-style: italic; }
span.dealerverbose { color: #ffff00; }
span.dealerbrief { color: #ffff00; }
span.system { color: #ffff00; font-weight: bold; }
</STYLE>
</HEAD>

<BODY><!--StartFragment--><SPAN class=dealerbrief channel=""dealerbrief"" timestamp=""1372028835""><BR>Welcome 
to table Fremont 1503 with Fremont</SPAN><SPAN class=system channel=""system"" 
timestamp=""1372028835""><BR>Invite Only Sit &amp; Go's are registering for the 
next 2 hours!</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028835""><BR>dejenerate has J<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top> and gets the button</SPAN><SPAN class=dealerbrief 
channel=""dealerbrief"" timestamp=""1372028835""><BR>Now playing Hold 
'em</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028835""><BR>Game starts, Hold 'em, hand #5269150</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>dejenerate posts small blind ($0.25)</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028835""><BR>sling1012 
posts big blind ($0.50)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>dejenerate folds</SPAN><SPAN class=dealerbrief 
channel=""dealerbrief"" timestamp=""1372028835""><BR>sling1012 wins 
$0.50</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028835""><BR>Game starts, Hold 'em, hand #5269186</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028835""><BR>sling1012 
posts small blind ($0.25)</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028835""><BR>dejenerate posts big blind 
($0.50)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>sling1012 calls</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028835""><BR>dejenerate checks</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028835""><BR>The flop is 
5<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top> 7<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top> 6<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>dejenerate checks</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028835""><BR>sling1012 checks</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028835""><BR>The turn is 
J<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>dejenerate bets $0.65</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028835""><BR>sling1012 raises to 
$2.80</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028835""><BR>dejenerate, it's your turn</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>dejenerate calls</SPAN><SPAN class=dealerbrief 
channel=""dealerbrief"" timestamp=""1372028835""><BR>The river is 5<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028835""><BR>dejenerate checks</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028835""><BR>sling1012 bets $5</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028848""><BR>dejenerate, 
it's your turn</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028853""><BR>dejenerate folds</SPAN><SPAN class=dealerbrief 
channel=""dealerbrief"" timestamp=""1372028855""><BR>sling1012 wins 
$6.31</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028863""><BR>Game starts, Hold 'em, hand #5269376</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028863""><BR>Walbert must 
wait until the button passes</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028863""><BR>BigdonkeyD must wait until the button 
passes</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028863""><BR>dejenerate posts small blind ($0.25)</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028863""><BR>sling1012 
posts big blind ($0.50)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028873""><BR>dejenerate raises to $1</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028876""><BR>sling1012 
calls</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028879""><BR>The flop is 4<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
align=top> 2<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
align=top> 9<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028885""><BR>sling1012 checks</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028895""><BR>dejenerate bets 
$1.10</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028900""><BR>sling1012 calls</SPAN><SPAN class=dealerbrief 
channel=""dealerbrief"" timestamp=""1372028902""><BR>The turn is 4<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028911""><BR>sling1012 bets $2.50</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028915""><BR>dejenerate folds</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028917""><BR>sling1012 
wins $4.02</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028925""><BR>Game starts, Hold 'em, hand #5269509</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028925""><BR>dejenerate posts small blind ($0.25)</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028926""><BR>Walbert 
posts big blind ($0.50)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028926""><BR>BigdonkeyD posts ($0.50)</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028929""><BR>Your hole 
cards are 5<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top> 9<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028930""><BR>BigdonkeyD raises to $2</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028933""><BR>sling1012 
calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028938""><BR>dejenerate folds</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028946""><BR>Walbert folds</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028950""><BR>The flop is 
3<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top> 7<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top> 2<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028952""><BR>BigdonkeyD bets $2.50</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028956""><BR>sling1012 folds</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028958""><BR>BigdonkeyD 
wins $4.54</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028963""><BR>Game starts, Hold 'em, hand #5269575</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028963""><BR>Walbert 
posts small blind ($0.25)</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028963""><BR>blazn777 posts big blind 
($0.50)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028963""><BR>Belthazar666 posts ($0.50)</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372028967""><BR>Your hole 
cards are K<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
align=top> Q<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028968""><BR>Belthazar666 checks</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028968""><BR>BigdonkeyD folds</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028972""><BR>sling1012 
calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372028979""><BR>dejenerate folds</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372028983""><BR>Walbert raises to 
$3</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372028996""><BR>blazn777, it's your turn</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372028999""><BR>blazn777 
folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029001""><BR>Belthazar666 calls</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372029001""><BR>sling1012 calls</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372029004""><BR>The flop is 
Q<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
align=top> 2<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top> 7<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029008""><BR>Walbert bets $3.50</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372029015""><BR>Belthazar666 
folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029015""><BR>sling1012 calls</SPAN><SPAN class=dealerbrief 
channel=""dealerbrief"" timestamp=""1372029017""><BR>The turn is 6<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029026""><BR>Walbert bets $3 (all-in)</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" timestamp=""1372029030""><BR>sling1012 
calls</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372029033""><BR>The river is J<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372029038""><BR>sling1012 shows 7<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
align=top> 8<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372029038""><BR>Walbert shows K<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
align=top> Q<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
timestamp=""1372029043""><BR>Walbert wins $21.49 with a pair of queens</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372029047""><BR>Game starts, 
Hold 'em, hand #5269745</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029047""><BR>blazn777 posts small blind ($0.25)</SPAN><SPAN 
class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029047""><BR>Belthazar666 posts big blind ($0.50)</SPAN><SPAN 
class=dealerbrief channel=""dealerbrief"" timestamp=""1372029051""><BR>Your hole 
cards are K<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
align=top> A<IMG 
src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
timestamp=""1372029056""><BR>BigdonkeyD calls</SPAN><SPAN class=dealerverbose 
channel=""dealerverbose"" timestamp=""1372029065""><BR>sling1012 raises to $3</SPAN><!--EndFragment--></BODY>

</HTML>

";
        string _html1 = @"
            Version:1.0
            StartHTML:000000234
            EndHTML:000027522
            StartFragment:000000961
            EndFragment:000027484
            StartSelection:000000961
            EndSelection:000027484
            SourceURL:file://C:\Documents and Settings\Rodney\Local Settings\Temp\ggp274E.htm
            <!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">

            <HTML><HEAD>
            <SCRIPT language=Javascript>
            document.write('<BASE href=""');
            document.write(window.external.imagesPath);
            document.write('"">');
            </SCRIPT>
            <BASE 
            href=""C:/Documents and Settings/All Users/Application Data/Ultimate Gaming/Ultimate Poker/images/"">
            <STYLE type=text/css>
            body { background-color: #000000; margin: 1px; word-wrap: break-word; }
            span { color: #ffffff; font-family: Arial; font-size: 11px; }
            span.player { }
            span.observer { color: #ff0000; font-style: italic; }
            span.dealerverbose { color: #ffff00; }
            span.dealerbrief { color: #ffff00; }
            span.system { color: #ffff00; font-weight: bold; }
            </STYLE>
            </HEAD>

            <BODY><!--StartFragment--><SPAN class=dealerbrief channel=""dealerbrief"" timestamp=""1371963268""><BR>Welcome 
            to table Flamingo 986 with Flamingo</SPAN><SPAN class=system channel=""system"" 
            timestamp=""1371963268""><BR>Chat with New Heights a ""Real Boss"" table Desert 
            Inn</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963280""><BR>Walbert has 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> and gets the button</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963283""><BR>Now playing Hold 
            'em</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963283""><BR>Game starts, Hold 'em, hand #5184525</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963283"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963284"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963285""><BR>Your hole cards are 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963289"" content=""<BR>Walbert calls""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963295"" 
            content=""<BR>mdh checks""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963299""><BR>The flop is Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963303"" content=""<BR>mdh checks""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963306"" 
            content=""<BR>Walbert checks""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963307""><BR>The turn is 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963311"" content=""<BR>mdh bets $1""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963316"" 
            content=""<BR>Walbert folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963318""><BR>mdh wins $1.91</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963322""><BR>Game starts, Hold 'em, hand 
            #5184626</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963322"" content=""<BR>mdh posts small blind ($0.50)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963322"" 
            content=""<BR>Walbert posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963324""><BR>Your hole cards are K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963328"" content=""<BR>mdh calls""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963330"" 
            content=""<BR>Walbert checks""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963334""><BR>The flop is J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963336"" content=""<BR>Walbert checks""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963338"" 
            content=""<BR>mdh bets $1""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963340"" 
            content=""<BR>Walbert calls""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963342""><BR>The turn is 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963345"" content=""<BR>Walbert checks""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963349"" 
            content=""<BR>mdh checks""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963351""><BR>The river is Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963364""><BR>Walbert, it's your turn</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963366"" 
            content=""<BR>Walbert checks""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963368"" 
            content=""<BR>mdh bets $1""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963373"" 
            content=""<BR>Walbert folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963375""><BR>mdh wins $3.82</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963379""><BR>Game starts, Hold 'em, hand 
            #5184763</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963380"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963380"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963381""><BR>Your hole cards are J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963385"" content=""<BR>Walbert calls""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963395"" 
            content=""<BR>mdh raises to $2""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963397"" 
            content=""<BR>Walbert calls""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963400""><BR>The flop is Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963403"" content=""<BR>mdh bets $1""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963407"" 
            content=""<BR>Walbert folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963409""><BR>mdh wins $3.82</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963414""><BR>Game starts, Hold 'em, hand 
            #5184840</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963414"" content=""<BR>mdh posts small blind ($0.50)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963414"" 
            content=""<BR>Walbert posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963415""><BR>Your hole cards are Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963418"" content=""<BR>mdh raises to $2""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963422"" 
            content=""<BR>Walbert raises to $7""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963424"" 
            content=""<BR>mdh calls""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963428""><BR>The flop is 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963431"" content=""<BR>Walbert bets $8 (all-in)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963435"" 
            content=""<BR>mdh calls""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963438""><BR>The turn is 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963441""><BR>The river is 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963446""><BR>Walbert shows Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963446""><BR>mdh shows A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963451""><BR>Walbert wins $29.50 with ace high</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371963455""><BR>Game starts, 
            Hold 'em, hand #5184939</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963455"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963455"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963456""><BR>Your hole cards are 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963458"" content=""<BR>Walbert calls""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963460"" 
            content=""<BR>mdh checks""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963463""><BR>The flop is 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963465"" content=""<BR>mdh checks""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963469"" 
            content=""<BR>Walbert bets $1""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963474"" 
            content=""<BR>mdh folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963476""><BR>Walbert wins $1.91</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963480""><BR>Game starts, Hold 'em, hand 
            #5185009</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963481"" content=""<BR>mdh posts small blind ($0.50)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963481"" 
            content=""<BR>Walbert posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963482""><BR>Your hole cards are A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963484"" content=""<BR>mdh folds""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963487""><BR>Walbert wins $1</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371963491""><BR>Game starts, 
            Hold 'em, hand #5185043</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963491"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963491"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963492""><BR>Your hole cards are K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963495"" content=""<BR>Walbert calls""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963498"" 
            content=""<BR>mdh checks""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963501""><BR>The flop is 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963504"" content=""<BR>mdh bets $1""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963508"" 
            content=""<BR>Walbert folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963510""><BR>mdh wins $1.91</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963514""><BR>Game starts, Hold 'em, hand 
            #5185092</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963515"" content=""<BR>mdh posts small blind ($0.50)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963515"" 
            content=""<BR>Walbert posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963516""><BR>Your hole cards are 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963522"" content=""<BR>mdh calls""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963525"" 
            content=""<BR>Walbert checks""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963528""><BR>The flop is A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963531"" content=""<BR>Walbert checks""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963533"" 
            content=""<BR>mdh bets $1""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963537"" 
            content=""<BR>Walbert folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963539""><BR>mdh wins $1.91</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963543""><BR>Game starts, Hold 'em, hand 
            #5185172</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963544"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963544"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963545""><BR>Your hole cards are 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963549"" content=""<BR>Walbert calls""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963555"" 
            content=""<BR>mdh checks""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963558""><BR>The flop is 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963561"" content=""<BR>mdh checks""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963563"" 
            content=""<BR>Walbert bets $1""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963566"" 
            content=""<BR>mdh folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963569""><BR>Walbert wins $1.91</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963573""><BR>Game starts, Hold 'em, hand 
            #5185246</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963573"" content=""<BR>mdh posts small blind ($0.50)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963573"" 
            content=""<BR>Walbert posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963574""><BR>Your hole cards are 3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963581"" content=""<BR>mdh calls""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963584"" 
            content=""<BR>Walbert checks""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963587""><BR>The flop is K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963591"" content=""<BR>Walbert bets $1""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963596"" 
            content=""<BR>mdh folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963599""><BR>Walbert wins $1.91</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963603""><BR>Game starts, Hold 'em, hand 
            #5185331</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963603"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963603"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963605""><BR>Your hole cards are 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963609"" content=""<BR>Walbert raises to $2""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963615"" 
            content=""<BR>mdh folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963618""><BR>Walbert wins $2</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963622""><BR>Game starts, Hold 'em, hand 
            #5185378</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963622"" content=""<BR>mdh posts small blind ($0.50)""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963622"" 
            content=""<BR>Walbert posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963624""><BR>Your hole cards are 3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963629"" content=""<BR>mdh calls""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963631"" 
            content=""<BR>Walbert checks""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963635""><BR>The flop is 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963637"" content=""<BR>Walbert checks""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963639"" 
            content=""<BR>mdh bets $1""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963642"" 
            content=""<BR>Walbert calls""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963644""><BR>The turn is 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963646"" content=""<BR>Walbert checks""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371963650"" 
            content=""<BR>mdh bets $4.87 (all-in)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963653"" 
            content=""<BR>Walbert folds""></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371963656""><BR>mdh wins $3.82</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963660""><BR>Game starts, Hold 'em, hand 
            #5185476</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371963660"" 
            content=""<BR>Walbert posts small blind ($0.50)""></SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371963660"" 
            content=""<BR>mdh posts big blind ($1)""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371963661""><BR>Your hole cards are 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><!--EndFragment--></BODY>

            </HTML>";
        string _html = @"
            <HTML><HEAD>
            <SCRIPT language=Javascript>
            docuent.write('<BASE href=""');
            document.write(window.external.imagesPath);
            document.write('"">');
            </SCRIPT>
            <BASE 
            href=""C:/Documents and Settings/All Users/Application Data/Ultimate Gaming/Ultimate Poker/images/"">
            <STYLE type=text/css>
            body { background-color: #000000; margin: 1px; word-wrap: break-word; }
            span { color: #ffffff; font-family: Arial; font-size: 11px; }
            span.player { }
            span.observer { color: #ff0000; font-style: italic; }
            span.dealerverbose { color: #ffff00; }
            span.dealerbrief { color: #ffff00; }
            span.system { color: #ffff00; font-weight: bold; }
            </STYLE>
            </HEAD>

            <BODY><!--StartFragment--><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>MJLebron236 calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690147""><BR>The flop is 
            4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>The turn is 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>The river is 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>MJLebron236 shows Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>SteveKelly shows 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>MJLebron236 wins $252 with two pair, queens and 
            eights</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>Game starts, Hold 'em, hand #4817351</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690147""><BR>GangnamX 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jakeaction posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690147""><BR>jspadde 
            raises to $24</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>YUCKOE folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>billbraski folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>SteveKelly raises to $120 
            (all-in)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>gothunder folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>GangnamX folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>jspadde calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690147""><BR>The flop is 
            2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>The turn is 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>The river is A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>jspadde shows K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>SteveKelly shows J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>jspadde wins $246 with two pair, aces and 
            kings</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690147""><BR>Game starts, Hold 'em, hand #4817431</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jakeaction posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690147""><BR>jspadde 
            posts big blind ($6)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>ShipIt posts ($6)</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>billbraski folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690147""><BR>ShipIt 
            checks</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>SteveKelly folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>gothunder folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690147""><BR>GangnamX 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jakeaction calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>jspadde checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690147""><BR>The flop is 
            K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jakeaction checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>jspadde bets $6</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690147""><BR>YUCKOE 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>ShipIt folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690147""><BR>The turn is 
            6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jspadde bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690147""><BR>YUCKOE raises to 
            $18</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690147""><BR>jspadde calls</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690147""><BR>The river is 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690148""><BR>jspadde bets $18</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690150""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690152""><BR>jspadde has 
            2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> and two pair, kings and sixes</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690153""><BR>YUCKOE has 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> and two pair, aces and sixes</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690154""><BR>YUCKOE wins $105 with two pair, 
            aces and sixes</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690158""><BR>Game starts, Hold 'em, hand #4817528</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690158""><BR>jspadde 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690161""><BR>YUCKOE posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690170""><BR>MJLebron236 calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690172""><BR>ShipIt calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690172""><BR>SteveKelly folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690178""><BR>gothunder raises to $105 
            (all-in)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690181""><BR>GangnamX folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690181""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690181""><BR>jspadde 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690187""><BR>YUCKOE folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690187""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690193""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690195""><BR>gothunder wins $27</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690199""><BR>Game starts, Hold 'em, hand 
            #4817592</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690201""><BR>YUCKOE posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690201""><BR>MJLebron236 posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690215""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690216""><BR>SteveKelly folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690216""><BR>gothunder folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690216""><BR>GangnamX 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690216""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690218""><BR>jspadde calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690224""><BR>YUCKOE 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690230""><BR>MJLebron236 checks</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690233""><BR>The flop is 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690237""><BR>YUCKOE bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690240""><BR>MJLebron236 raises to 
            $12</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690242""><BR>jspadde folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690245""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690246""><BR>The turn is 
            6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690252""><BR>YUCKOE bets $30</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690262""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690264""><BR>YUCKOE wins 
            $40.11</SPAN><SPAN class=player channel=""player"" timestamp=""1371690266"" 
            content=""<BR>MJLebron236:&amp;nbsp;king""></SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690270""><BR>YUCKOE shows 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> for a flush, king high</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690274""><BR>Game starts, Hold 'em, hand 
            #4817710</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690274""><BR>MJLebron236 posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690274""><BR>ShipIt 
            posts big blind ($6)</SPAN><SPAN class=player channel=""player"" 
            timestamp=""1371690279"" content=""<BR>MJLebron236:&amp;nbsp;vnh""></SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690279""><BR>SteveKelly raises to $12</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690282""><BR>gothunder 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690282""><BR>GangnamX folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690282""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690284""><BR>jspadde 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690288""><BR>YUCKOE folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690288""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690288""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690292""><BR>The flop is 3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690296""><BR>SteveKelly bets $13</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690300""><BR>gothunder calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690300""><BR>jspadde 
            calls</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690301""><BR>The turn is 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690307""><BR>SteveKelly checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690313""><BR>gothunder bets $101 
            (all-in)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690315""><BR>jspadde folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690320""><BR>SteveKelly folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690323""><BR>gothunder 
            wins $81</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690327""><BR>Game starts, Hold 'em, hand #4817793</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690327""><BR>ShipIt 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690330""><BR>gothunder posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690335""><BR>GangnamX 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690335""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690337""><BR>jspadde calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690339""><BR>YUCKOE 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690339""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690342""><BR>ShipIt folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690342""><BR>gothunder 
            checks</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690345""><BR>The flop is K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690347""><BR>gothunder checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690349""><BR>jspadde bets $18</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690349""><BR>YUCKOE 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690352""><BR>gothunder folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690354""><BR>jspadde wins $20.06</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690360""><BR>Game starts, 
            Hold 'em, hand #4817840</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690360""><BR>gothunder posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690361""><BR>jakeaction posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690365""><BR>jspadde 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690367""><BR>YUCKOE folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690373""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690376""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690376""><BR>gothunder calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690376""><BR>jakeaction checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690379""><BR>The flop is 
            4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690381""><BR>gothunder checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690383""><BR>jakeaction bets 
            $6</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690384""><BR>jspadde calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690389""><BR>gothunder calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690391""><BR>The turn is 
            3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690394""><BR>gothunder checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690395""><BR>jakeaction bets 
            $6</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690397""><BR>jspadde calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690400""><BR>gothunder calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690401""><BR>The river is 
            6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690413""><BR>gothunder bets $30</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690415""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690417""><BR>jspadde 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690419""><BR>gothunder wins $51.57</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690423""><BR>Game starts, Hold 'em, hand 
            #4817941</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690423""><BR>jakeaction posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690424""><BR>jspadde 
            posts big blind ($6)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690429""><BR>YUCKOE calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690435""><BR>MJLebron236 calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690440""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690440""><BR>gothunder calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690440""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690443""><BR>jspadde 
            checks</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690446""><BR>The flop is K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690450""><BR>jspadde checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690453""><BR>YUCKOE checks</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690461""><BR>MJLebron236 bets $13</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690465""><BR>gothunder folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690466""><BR>jspadde 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690471""><BR>YUCKOE folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690473""><BR>The turn is J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690477""><BR>jspadde bets $30</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690481""><BR>MJLebron236 calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690483""><BR>The river is 
            10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690489""><BR>jspadde bets $42</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690502""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690505""><BR>jspadde wins 
            $110</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690511""><BR>Game starts, Hold 'em, hand #4818096</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690512""><BR>jspadde 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690514""><BR>YUCKOE posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690525""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690525""><BR>ShipIt folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690530""><BR>gothunder 
            raises to $12</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690530""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690532""><BR>jspadde calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690537""><BR>YUCKOE 
            calls</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690541""><BR>The flop is 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690545""><BR>jspadde bets $30</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690558""><BR>YUCKOE, it's your 
            turn</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690565""><BR>YUCKOE folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690567""><BR>gothunder calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690569""><BR>The turn is 
            4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690574""><BR>jspadde bets $36</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690587""><BR>gothunder, it's your 
            turn</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690589""><BR>gothunder raises to $161.57 (all-in)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690592""><BR>jspadde 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690594""><BR>gothunder wins $165</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690598""><BR>Game starts, Hold 'em, hand 
            #4818249</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690605""><BR>YUCKOE posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690605""><BR>MJLebron236 posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690613""><BR>ShipIt 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690613""><BR>gothunder raises to $12</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690614""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690616""><BR>jspadde raises to 
            $54</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690626""><BR>YUCKOE calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690628""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690631""><BR>ShipIt 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690631""><BR>gothunder folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690634""><BR>The flop is A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690641""><BR>YUCKOE bets $115.11 (all-in)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690646""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690658""><BR>jspadde folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690660""><BR>YUCKOE wins $177</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690664""><BR>YUCKOE shows 
            Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> for two pair, aces and eights</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690668""><BR>Game starts, Hold 'em, hand 
            #4818384</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690668""><BR>MJLebron236 posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690668""><BR>ShipIt 
            posts big blind ($6)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690672""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690680""><BR>jspadde calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690683""><BR>YUCKOE 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690685""><BR>MJLebron236 calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690687""><BR>ShipIt checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690690""><BR>The flop is 
            A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690695""><BR>MJLebron236 checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690697""><BR>ShipIt bets $18</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690699""><BR>jspadde 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690701""><BR>YUCKOE folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690703""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690706""><BR>ShipIt wins 
            $22.92</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690710""><BR>Game starts, Hold 'em, hand #4818467</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690710""><BR>ShipIt 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690710""><BR>jakeaction posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690714""><BR>jspadde 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690725""><BR>YUCKOE, it's your turn</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690733""><BR>YUCKOE 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690740""><BR>MJLebron236 raises to $18</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690740""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690740""><BR>jakeaction folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690742""><BR>MJLebron236 wins 
            $15</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690746""><BR>Game starts, Hold 'em, hand #4818546</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690747""><BR>jakeaction posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690751""><BR>YUCKOE 
            posts big blind ($6)</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690765""><BR>MJLebron236, it's your turn</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690767""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690767""><BR>ShipIt folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690769""><BR>jakeaction calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690773""><BR>YUCKOE checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690776""><BR>The flop is 
            K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> 3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690778""><BR>jakeaction bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690781""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690783""><BR>The turn is 
            Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690785""><BR>jakeaction bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690790""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690792""><BR>The river is 
            K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690794""><BR>jakeaction bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690800""><BR>YUCKOE folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690802""><BR>jakeaction 
            wins $34.50</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690807""><BR>Game starts, Hold 'em, hand #4818656</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690812""><BR>YUCKOE 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690813""><BR>MJLebron236 posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690818""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690818""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690824""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690827""><BR>MJLebron236 checks</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690830""><BR>The flop is A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690837""><BR>YUCKOE checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690840""><BR>MJLebron236 bets 
            $6</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690843""><BR>Player YUCKOE lost connection. Please stand by while 
            we attempt to reconnect.</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690844""><BR>YUCKOE has reconnected and now has 20 seconds to 
            act</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690853""><BR>YUCKOE folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690855""><BR>MJLebron236 wins 
            $11.46</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690860""><BR>Game starts, Hold 'em, hand #4818746</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690860""><BR>MJLebron236 posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690860""><BR>ShipIt 
            posts big blind ($6)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690868""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690872""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690881""><BR>MJLebron236 raises to $24</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690889""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690893""><BR>Player YUCKOE lost connection. Please stand by while 
            we attempt to reconnect.</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690894""><BR>YUCKOE has reconnected and now has 20 seconds to 
            act</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690900""><BR>YUCKOE calls</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690903""><BR>The flop is 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 5<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690912""><BR>MJLebron236 bets $25</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690916""><BR>YUCKOE folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690918""><BR>MJLebron236 
            wins $52.50</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690923""><BR>Game starts, Hold 'em, hand #4818861</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690923""><BR>ShipIt 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690925""><BR>SteveKelly posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690929""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690931""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690934""><BR>MJLebron236 calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690944""><BR>ShipIt calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690951""><BR>SteveKelly checks</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371690955""><BR>The flop is 3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690958""><BR>ShipIt checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690959""><BR>SteveKelly checks</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371690962""><BR>YUCKOE 
            bets $18</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690967""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690972""><BR>ShipIt raises to $46.92 
            (all-in)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690975""><BR>SteveKelly folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371690978""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371690980""><BR>The turn is 
            3<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690984""><BR>The river is Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690989""><BR>YUCKOE shows 4<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690989""><BR>ShipIt shows 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690994""><BR>YUCKOE wins $114.84 with two pair, fours and 
            threes</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371690998""><BR>Game starts, Hold 'em, hand #4818989</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690998""><BR>SteveKelly posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371690998""><BR>jakeaction posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691003""><BR>YUCKOE 
            folds</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691006""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691012""><BR>ShipIt calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691017""><BR>SteveKelly calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691017""><BR>jakeaction checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371691020""><BR>The flop is 
            Q<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691022""><BR>SteveKelly checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691024""><BR>jakeaction checks</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691025""><BR>ShipIt 
            checks</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691027""><BR>The turn is 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691030""><BR>SteveKelly checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691032""><BR>jakeaction checks</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691035""><BR>ShipIt 
            bets $12</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691040""><BR>SteveKelly folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691040""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371691042""><BR>ShipIt wins 
            $17.19</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691047""><BR>Game starts, Hold 'em, hand #4819067</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691047""><BR>jakeaction posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691052""><BR>YUCKOE 
            posts big blind ($6)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691052""><BR>0Moloch0 posts ($6)</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691060""><BR>MJLebron236 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691063""><BR>0Moloch0 
            checks</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691063""><BR>ShipIt calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691063""><BR>SteveKelly folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691063""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691068""><BR>YUCKOE checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371691072""><BR>The flop is 
            9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 2<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691074""><BR>YUCKOE checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691077""><BR>0Moloch0 checks</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691079""><BR>ShipIt 
            checks</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691081""><BR>The turn is K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691086""><BR>YUCKOE bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691090""><BR>0Moloch0 folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691092""><BR>ShipIt 
            folds</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691094""><BR>YUCKOE wins $20.06</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371691103""><BR>Game starts, Hold 'em, hand 
            #4819164</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691106""><BR>YUCKOE posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691106""><BR>MJLebron236 posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691111""><BR>0Moloch0 
            raises to $15</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691113""><BR>ShipIt folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691115""><BR>SteveKelly folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691115""><BR>jakeaction folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691120""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691124""><BR>MJLebron236 folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371691127""><BR>The flop is 6<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 9<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691134""><BR>YUCKOE bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691140""><BR>0Moloch0 folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371691142""><BR>YUCKOE wins 
            $34.38</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691146""><BR>YUCKOE shows 10<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> 7<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_heart.png"" 
            align=top> for a pair of sevens</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371691150""><BR>Game starts, Hold 'em, hand 
            #4819246</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691150""><BR>MJLebron236 posts small blind ($3)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691150""><BR>0Moloch0 
            posts big blind ($6)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691159""><BR>ShipIt folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691159""><BR>SteveKelly folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691160""><BR>jakeaction calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691171""><BR>YUCKOE calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691173""><BR>MJLebron236 calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691173""><BR>0Moloch0 checks</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371691176""><BR>The flop is 
            J<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top> K<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_diamond.png"" 
            align=top> A<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_club.png"" 
            align=top></SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691190""><BR>MJLebron236, it's your turn</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691201""><BR>MJLebron236 checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691204""><BR>0Moloch0 checks</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691205""><BR>jakeaction bets $6</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691214""><BR>YUCKOE folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691221""><BR>MJLebron236 calls</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691230""><BR>0Moloch0 folds</SPAN><SPAN 
            class=dealerbrief channel=""dealerbrief"" timestamp=""1371691232""><BR>The turn is 
            8<IMG 
            src=""file:///C:/Documents%20and%20Settings/All%20Users/Application%20Data/Ultimate%20Gaming/Ultimate%20Poker/images/suit_spade.png"" 
            align=top></SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691236""><BR>MJLebron236 checks</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691238""><BR>jakeaction bets 
            $6</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691240""><BR>MJLebron236 folds</SPAN><SPAN class=dealerbrief 
            channel=""dealerbrief"" timestamp=""1371691242""><BR>jakeaction wins 
            $34.38</SPAN><SPAN class=dealerbrief channel=""dealerbrief"" 
            timestamp=""1371691247""><BR>Game starts, Hold 'em, hand #4819419</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691247""><BR>0Moloch0 
            posts small blind ($3)</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691247""><BR>ShipIt posts big blind ($6)</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691252""><BR>SteveKelly folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691252""><BR>jakeaction folds</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691259""><BR>YUCKOE 
            calls</SPAN><SPAN class=dealerverbose channel=""dealerverbose"" 
            timestamp=""1371691262""><BR>MJLebron236 folds</SPAN><SPAN class=dealerverbose 
            channel=""dealerverbose"" timestamp=""1371691264""><BR>0Moloch0 calls</SPAN><SPAN 
            class=dealerverbose channel=""dealerverbose"" timestamp=""1371691272""><BR>ShipIt 
            checks</SPAN><!--EndFragment--></BODY>

            </HTML>
            "; 
        #endregion
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void CanGetHand()
        {

            Parser p = new Parser(new NullLogger());
            p.UpdateWithLatestHtml(_html2);

            Assert.IsNotNull(p.Hand);
            
        }
        [TestMethod]
        public void CanGetPlayerCount()
        {

            Parser p = new Parser(new NullLogger());
            
            p.UpdateWithLatestHtml(_html2);
            p.InferPlayerCount(_html2);

            Assert.IsNotNull(p.Hand);

        }



        

    }
}
