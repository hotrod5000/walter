﻿using Bot.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Bot.Test
{


    /// <summary>
    ///This is a test class for HoldemHandTest and is intended
    ///to contain all HoldemHandTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HoldemHandTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AddCard
        ///</summary>
        [TestMethod()]
        public void HandDealtEventFiresWhenSecondCardIsDealt()
        {
            string card1 = "3s";
            string card2 = "3h";
            AutoResetEvent resetEvent = new AutoResetEvent(false);
            HoldemHand sut = new HoldemHand(); // TODO: Initialize to an appropriate value
            bool eventFired = false;
            int cardCount = 0;
            sut.HandDealt += new HandDealtEventHandler(delegate(object sender, HandDealtEventArgs e)
                {
                    var cards = new List<string>{e.Card1, e.Card2};
                    
                    Assert.IsTrue(cards.Any(x=>x == card1));
                    Assert.IsTrue(cards.Any(x=>x == card2));
                    eventFired = true;
                    resetEvent.Set();
                });
            
            sut.AddCard(card1);
            cardCount++;
            sut.AddCard(card2);
            cardCount++;
            resetEvent.WaitOne();
            Assert.IsTrue(eventFired);

        }

    }
}
