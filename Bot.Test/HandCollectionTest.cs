﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bot.Core.Objects;
using Bot.Core;

namespace Bot.Test
{
    /// <summary>
    /// Summary description for HandCollectionTest
    /// </summary>
    [TestClass]
    public class HandCollectionTest
    {
        public HandCollectionTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CanSerializeHandCollection()
        {
            var hands = new Bot.Core.Objects.HandCollection(new HandGroupPreference());

            hands.Set(3, 3, false, 2);
            string fileName = "hands.cfg";
            SerializationMethods.SerializeStartingHands(hands, fileName);
            var sut = SerializationMethods.DeserializeStartingHands(fileName);
            Assert.AreEqual(2,sut.GetGroupNumber(3, 3, false));
            
            
            
        }
    }
}
