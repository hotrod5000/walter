﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bot.Core.Objects;

namespace Bot.Test
{
    [TestClass]
    public class GameModelTest
    {
        private void DealToPlayers(GameModel m, int numberOfPlayers)
        {
            for (int i = 0; i < numberOfPlayers; i++)
                m.DealHandToPlayerAtSeatNumber(i);
        }
        [TestMethod]
        public void GetPositionValue()
        {
            GameModel sut = new GameModel();
            sut.Initialize();
            DealToPlayers(sut, 4);
            sut.DealerSeatNumber = 3;
            sut.MySeatNumber = 3;
            Assert.AreEqual(4, sut.PositionValue);


        }
        [TestMethod]
        public void GameModelIsValueType()
        {
            GameModel m = new GameModel();
            m.Initialize();
            GameModel m2 = m;
            m2.MySeatNumber = 6;
            m.MySeatNumber = 5;
            Assert.AreNotEqual(m2.MySeatNumber, m.MySeatNumber);
            Assert.AreEqual(6, m2.MySeatNumber);
            Assert.AreEqual(5, m.MySeatNumber);
        }
    }
}
