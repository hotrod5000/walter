﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Reflection;

namespace WindowsFormsApplication1
{
    public struct RECT
    {
        public int Left, Top, Right, Bottom;
        public RECT(Rectangle r)
        {
            this.Left = r.Left;
            this.Top = r.Top;
            this.Bottom = r.Bottom;
            this.Right = r.Right;
        }
    }
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern int DrawText(IntPtr hdc, string lpStr, int nCount, ref RECT lpRect, int wFormat);

        string _tourneyId = string.Empty;
        IntPtr _handle;
        public bool ButtonTourneyTableChangeEnabled
        {
            get { return buttonTourneyTableChange.Enabled; }
            set { buttonTourneyTableChange.Enabled = value; }
        }
        private Form1()
        {
            InitializeComponent();
        }
        public Form1(TextWriter tw)
            : this()
        {
            _textWriter = tw;
        }
        public Form1(TextWriter tw,string tourneyId):this()
        {
            _tourneyId = tourneyId;
            _textWriter = tw;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _handle = this.Handle;
            textBoxHandle.Text = _handle.ToString("X8");
            textBoxProcessId.Text = Process.GetCurrentProcess().Id.ToString();
        }
        private void WriteRequestAction()
        {
            var a = string.Format("MSG_TABLE_REQUESTACTION {0}", _handle.ToString("X8"));
            _textWriter.WriteLine(a);
            //if we are in the big blind, make it free to check
            
            _textWriter.WriteLine(" 'F' 0");
            if (GetPositionValue() == 2)
            {
                _textWriter.WriteLine(" 'c' 0");
            }
            else
            {
                _textWriter.WriteLine(" 'c' 20");
            }
            _textWriter.WriteLine(" '*' 'E' 40, 1500, 20, 1");
            _textWriter.WriteLine("MSG_TABLE_REQUESTACTION_REPLY posted");

        }
        TextWriter _textWriter;// = new StreamWriter("date.txt");
        int dealerPos = 0;
        private void buttonDealCards_Click(object sender, EventArgs e)
        {
            pokerTable1.AdvanceDealerButton();

            //parse the cards from the text box
            string input = textBox1.Text;
            var cards = input.Split(' ');
            string card1 = cards[0];
            string card2 = cards[1];

            var a = string.Format("MSG_TABLE_PLAYERCARDS {0}", _handle.ToString("X8"));
            var b = string.Format("::: {0}", card1);
            var c = string.Format("::: {0}", card2);
            var d = string.Format("------ {0}", _handle.ToString("X8"));

            _textWriter.WriteLine(a);
            _textWriter.WriteLine(b);
            _textWriter.WriteLine(c);
            _textWriter.WriteLine(d);

            _textWriter.WriteLine("MSG_TABLE_SUBSCR_DEALPLAYERCARDS");
            for (int i = 0; i < pokerTable1.PlayerCount; i++)
            {
                if (pokerTable1.PlayerIsToBeDealtIn(i))
                {
                    _textWriter.WriteLine(string.Format("sit{0}", i));
                    _textWriter.WriteLine("nCards=2");
                }
            }


            _textWriter.WriteLine(string.Format("dealerPos={0}", pokerTable1.DealerSeatNumber));

           
  
          
            

            WriteRequestAction();
            string stats = string.Format("Statistics 00000005 for {0}", _handle.ToString("X8"));
            _textWriter.WriteLine(stats);

            

        }
        public int GetPositionValue()
        {
            List<int> list = new List<int>();
            for(int i=0;i<pokerTable1.PlayerCount;i++)
            {
                if (pokerTable1.PlayerIsToBeDealtIn(i))
                    list.Add(i);
            }
            //start with dealer and count until you get to me
            int dealerIndex = -1;
            int mySeatIndex = -1;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == pokerTable1.DealerSeatNumber) dealerIndex = i;
                if (list[i] == MySeatNumber) mySeatIndex = i;
            }
            int diff;
            if (dealerIndex <= mySeatIndex)
                diff = mySeatIndex - dealerIndex;
            else
                diff = list.Count - Math.Abs(mySeatIndex - dealerIndex);

            if (diff == 0) return list.Count;
            else return diff;

        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var origColor = button2.ForeColor;
            button2.ForeColor = Color.Red;
            //Thread.Sleep(1000);
            //button2.ForeColor = origColor;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var newWindow = new Form1(_textWriter) { Text = DateTime.Now.ToLongTimeString()};
            newWindow.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var tourneyId = _tourneyId == string.Empty ? _handle.ToString() : _tourneyId;
            var newWindow = new Form1(_textWriter, tourneyId) { Text = DateTime.Now.ToLongTimeString() + " Tournament " + _tourneyId };
            
            newWindow.Show();
            this.Close();
        }
        int MySeatNumber
        {
            get
            {
                int mySeatNumber;
                int.TryParse(textBoxMySeatNumber.Text, out mySeatNumber);
                return mySeatNumber;
            }
        }
        private void buttonSitAtTable_Click(object sender, EventArgs e)
        {

            pokerTable1.HighlightSeat(MySeatNumber);
            _textWriter.WriteLine(string.Format("MSG_TABLE_LOGIN_REPLY {0} sit={1} out=1 priv=0 text='' autoRebuy='-1 0 0 0'",_handle.ToString("X8"),MySeatNumber));
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        BackgroundWorker _worker = new BackgroundWorker();
        void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //open file
                var file = e.Argument.ToString();
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
                using (StreamReader r = new StreamReader(fs) )
                {
                    bool triggerSet = false;
                    string line;
                    // Read and display lines from the file until the end of 
                    // the file is reached.
                    while ((line = r.ReadLine()) != null && !_worker.CancellationPending)
                    {
                        
                        while (_backgroundWorkerIsPaused) { };
                        if (line.Contains("MSG_TABLE_LOGIN_REPLY"))
                            triggerSet = true;
                        if (triggerSet)
                        {
                            if (line.Contains("MSG_TABLE_SUBSCR_BEGINHAND")) _backgroundWorkerIsPaused = true;
                            _textWriter.WriteLine(line);
                            Thread.Sleep(100);
                        }
                        //foreach (var item in _keywords)
                        //{
                        //    if(line.Contains(item))
                        //        _textWriter.WriteLine(line);
                        //}

                    }
                    if (_worker.CancellationPending)
                        e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
        bool _backgroundWorkerIsPaused = false;
        
        
        private void playFromFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _worker.WorkerSupportsCancellation = true;
            if (_worker.IsBusy)
            {
                _worker.CancelAsync();
                playFromFileToolStripMenuItem.Text = "&Play from file...";
            }
            else
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.InitialDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                dlg.FileName = "PokerStars.log.0";
                var result = dlg.ShowDialog();
                var fileName = string.Empty;
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    fileName = dlg.FileName;

                    
                    _worker.DoWork += new DoWorkEventHandler(_worker_DoWork);
                    _worker.RunWorkerAsync(fileName);
                }
                playFromFileToolStripMenuItem.Text = "&Stop playing from file";
            }
        }
        private void PauseOrResume()
        {
            _backgroundWorkerIsPaused = !_backgroundWorkerIsPaused;
            if (_backgroundWorkerIsPaused)
            {
                buttonPauseAndResume.Text = ">";
                buttonPauseAndResume.BackColor = Color.Green;
            }
            else
            {
                buttonPauseAndResume.Text = "''";
                buttonPauseAndResume.BackColor = Color.Red;
            }
        }
        private void buttonPauseAndResume_Click(object sender, EventArgs e)
        {
            PauseOrResume();
        }

        
    }
}
