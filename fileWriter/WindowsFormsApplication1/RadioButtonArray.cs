﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WindowsFormsApplication1
{
    public class RadioButtonArray : System.Collections.CollectionBase
    {
        private readonly System.Windows.Forms.Panel HostForm;
        // C# 
        // Replace the default constructor with this one.
        public RadioButtonArray(System.Windows.Forms.Panel host)
        {
            HostForm = host;
            
        }
        // C#
        public System.Windows.Forms.RadioButton this[int Index]
        {
            get
            {
                return (System.Windows.Forms.RadioButton)this.List[Index];
            }
        }
        // C#
        public void ClickHandler(Object sender, System.EventArgs e)
        {
            
        }

        // C#
        public void Remove()
        {
            // Check to be sure there is a button to remove.
            if (this.Count > 0)
            {
                // Remove the last button added to the array from the host form 
                // controls collection. Note the use of the indexer in accessing 
                // the array.
                HostForm.Controls.Remove(this[this.Count - 1]);
                this.List.RemoveAt(this.Count - 1);
            }
        }
        public int SelectedIndex
        {
            get
            {
                for (int i = 0; i < this.List.Count; i++)
                {
                    if ((this.List[i] as RadioButton).Checked) return i;
                }
                return -1;
            }
        }
        // C# 
        public System.Windows.Forms.RadioButton AddNewRadioButton(int top, int left)
        {
            // Create a new instance of the Button class.
            System.Windows.Forms.RadioButton aCheckbox = new
               System.Windows.Forms.RadioButton();
            // Add the button to the collection's internal list.
            this.List.Add(aCheckbox);
            // Add the button to the controls collection of the form 
            // referenced by the HostForm field.
            HostForm.Controls.Add(aCheckbox);
            // Set intial properties for the button object.
            aCheckbox.Top = top;
            aCheckbox.Left = left;
            aCheckbox.Tag = this.Count-1;
            //aCheckbox.Text = (this.Count-1).ToString();

            // C#
            aCheckbox.Click += new System.EventHandler(ClickHandler);

            aCheckbox.CheckedChanged += new EventHandler(aCheckbox_CheckedChanged);

            return aCheckbox;
        }

        void aCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            var radio = sender as RadioButton;
            if (radio.Checked)
            {
                radio.BackColor = Color.Red;
            }
            else
            {
                radio.BackColor = radio.Parent.BackColor;
            }
        }

    }
}
