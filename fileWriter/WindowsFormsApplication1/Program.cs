﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var writer = new MyStreamWriter("PokerStars.fileWriter.log.0");
            Application.Run(new Form1(writer) { ButtonTourneyTableChangeEnabled = false });
            //Application.Run(new controlarrayform());
            writer.Close();
        }
        public class MyStreamWriter : StreamWriter
        {
            public MyStreamWriter(string file) : base(file)
            {

            }
            public override void WriteLine(string value)
            {
                Console.WriteLine(value);
                base.WriteLine(value);
                Flush();
            }
        }
    }
}
