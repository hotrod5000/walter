﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonDealCards = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxProcessId = new System.Windows.Forms.TextBox();
            this.textBoxHandle = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonTourneyTableChange = new System.Windows.Forms.Button();
            this.buttonSitAtTable = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMySeatNumber = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playFromFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonPauseAndResume = new System.Windows.Forms.Button();
            this.pokerTable1 = new WindowsFormsApplication1.PokerTable();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonDealCards
            // 
            this.buttonDealCards.Location = new System.Drawing.Point(185, 126);
            this.buttonDealCards.Name = "buttonDealCards";
            this.buttonDealCards.Size = new System.Drawing.Size(75, 23);
            this.buttonDealCards.TabIndex = 0;
            this.buttonDealCards.Text = "Deal cards";
            this.buttonDealCards.UseVisualStyleBackColor = true;
            this.buttonDealCards.Click += new System.EventHandler(this.buttonDealCards_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(81, 128);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(98, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "14h 13h";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "process id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "window handle:";
            // 
            // textBoxProcessId
            // 
            this.textBoxProcessId.Location = new System.Drawing.Point(132, 21);
            this.textBoxProcessId.Name = "textBoxProcessId";
            this.textBoxProcessId.Size = new System.Drawing.Size(100, 20);
            this.textBoxProcessId.TabIndex = 4;
            // 
            // textBoxHandle
            // 
            this.textBoxHandle.Location = new System.Drawing.Point(132, 47);
            this.textBoxHandle.Name = "textBoxHandle";
            this.textBoxHandle.Size = new System.Drawing.Size(100, 20);
            this.textBoxHandle.TabIndex = 5;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(28, 179);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(28, 213);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(28, 261);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(204, 42);
            this.trackBar1.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(208, 299);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "new window";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(146, 179);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(80, 17);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "checkBox2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "cards";
            // 
            // buttonTourneyTableChange
            // 
            this.buttonTourneyTableChange.Location = new System.Drawing.Point(28, 299);
            this.buttonTourneyTableChange.Name = "buttonTourneyTableChange";
            this.buttonTourneyTableChange.Size = new System.Drawing.Size(126, 23);
            this.buttonTourneyTableChange.TabIndex = 12;
            this.buttonTourneyTableChange.Text = "Tourney table change";
            this.buttonTourneyTableChange.UseVisualStyleBackColor = true;
            this.buttonTourneyTableChange.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonSitAtTable
            // 
            this.buttonSitAtTable.Location = new System.Drawing.Point(254, 47);
            this.buttonSitAtTable.Name = "buttonSitAtTable";
            this.buttonSitAtTable.Size = new System.Drawing.Size(75, 23);
            this.buttonSitAtTable.TabIndex = 13;
            this.buttonSitAtTable.Text = "Sit down at table";
            this.buttonSitAtTable.UseVisualStyleBackColor = true;
            this.buttonSitAtTable.Click += new System.EventHandler(this.buttonSitAtTable_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(388, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "My seat #";
            // 
            // textBoxMySeatNumber
            // 
            this.textBoxMySeatNumber.Location = new System.Drawing.Point(453, 17);
            this.textBoxMySeatNumber.Name = "textBoxMySeatNumber";
            this.textBoxMySeatNumber.Size = new System.Drawing.Size(101, 20);
            this.textBoxMySeatNumber.TabIndex = 16;
            this.textBoxMySeatNumber.Text = "0";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(774, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playFromFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // playFromFileToolStripMenuItem
            // 
            this.playFromFileToolStripMenuItem.Name = "playFromFileToolStripMenuItem";
            this.playFromFileToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.playFromFileToolStripMenuItem.Text = "&Play from file...";
            this.playFromFileToolStripMenuItem.Click += new System.EventHandler(this.playFromFileToolStripMenuItem_Click);
            // 
            // buttonPauseAndResume
            // 
            this.buttonPauseAndResume.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonPauseAndResume.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPauseAndResume.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonPauseAndResume.Location = new System.Drawing.Point(12, 27);
            this.buttonPauseAndResume.Name = "buttonPauseAndResume";
            this.buttonPauseAndResume.Size = new System.Drawing.Size(24, 23);
            this.buttonPauseAndResume.TabIndex = 18;
            this.buttonPauseAndResume.Text = ">";
            this.buttonPauseAndResume.UseVisualStyleBackColor = false;
            this.buttonPauseAndResume.Click += new System.EventHandler(this.buttonPauseAndResume_Click);
            // 
            // pokerTable1
            // 
            this.pokerTable1.Location = new System.Drawing.Point(385, 47);
            this.pokerTable1.Name = "pokerTable1";
            this.pokerTable1.Size = new System.Drawing.Size(332, 235);
            this.pokerTable1.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 334);
            this.Controls.Add(this.buttonPauseAndResume);
            this.Controls.Add(this.textBoxMySeatNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pokerTable1);
            this.Controls.Add(this.buttonSitAtTable);
            this.Controls.Add(this.buttonTourneyTableChange);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBoxHandle);
            this.Controls.Add(this.textBoxProcessId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonDealCards);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDealCards;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxProcessId;
        private System.Windows.Forms.TextBox textBoxHandle;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonTourneyTableChange;

        
        private System.Windows.Forms.Button buttonSitAtTable;
        private PokerTable pokerTable1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxMySeatNumber;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playFromFileToolStripMenuItem;
        private System.Windows.Forms.Button buttonPauseAndResume;
    }
}

