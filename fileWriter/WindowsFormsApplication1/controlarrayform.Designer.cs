﻿namespace WindowsFormsApplication1
{
    partial class controlarrayform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pokerTable1 = new WindowsFormsApplication1.PokerTable();
            this.SuspendLayout();
            // 
            // pokerTable1
            // 
            this.pokerTable1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pokerTable1.Location = new System.Drawing.Point(0, 0);
            this.pokerTable1.Name = "pokerTable1";
            this.pokerTable1.Size = new System.Drawing.Size(648, 262);
            this.pokerTable1.TabIndex = 0;
            this.pokerTable1.Paint += new System.Windows.Forms.PaintEventHandler(this.pokerTable1_Paint);
            // 
            // controlarrayform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 262);
            this.Controls.Add(this.pokerTable1);
            this.Name = "controlarrayform";
            this.Text = "controlarrayform";
            this.ResumeLayout(false);

        }

        #endregion

        private PokerTable pokerTable1;

    }
}