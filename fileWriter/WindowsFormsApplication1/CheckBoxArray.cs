﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    public class CheckBoxArray : System.Collections.CollectionBase
    {
        private readonly System.Windows.Forms.Panel HostForm;
        // C# 
        // Replace the default constructor with this one.
        public CheckBoxArray(System.Windows.Forms.Panel host)
        {
            HostForm = host;
            
        }
        // C#
        public System.Windows.Forms.CheckBox this[int Index]
        {
            get
            {
                return (System.Windows.Forms.CheckBox)this.List[Index];
            }
        }
        // C#
        public void ClickHandler(Object sender, System.EventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show("You have clicked check box " +
            //   ((System.Windows.Forms.CheckBox)sender).Tag.ToString());
        }

        // C#
        public void Remove()
        {
            // Check to be sure there is a button to remove.
            if (this.Count > 0)
            {
                // Remove the last button added to the array from the host form 
                // controls collection. Note the use of the indexer in accessing 
                // the array.
                HostForm.Controls.Remove(this[this.Count - 1]);
                this.List.RemoveAt(this.Count - 1);
            }
        }

        // C# 
        public System.Windows.Forms.CheckBox AddNewCheckbox(int top, int left)
        {
            // Create a new instance of the Button class.
            System.Windows.Forms.CheckBox aCheckbox = new
               System.Windows.Forms.CheckBox();
            // Add the button to the collection's internal list.
            this.List.Add(aCheckbox);
            // Add the button to the controls collection of the form 
            // referenced by the HostForm field.
            HostForm.Controls.Add(aCheckbox);
            // Set intial properties for the button object.
            aCheckbox.Top = top;
            aCheckbox.Left = left;
            aCheckbox.Tag = this.Count-1;
            aCheckbox.Text = (this.Count-1).ToString();

            // C#
            aCheckbox.Click += new System.EventHandler(ClickHandler);

            return aCheckbox;
        }

    }
}
