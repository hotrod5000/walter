﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public class PokerTable : Panel
    {
        CheckBoxArray _checkBoxes;
        RadioButtonArray _radioButtons;
        public readonly int PlayerCount = 8;
        public PokerTable() : base()
        {
            _checkBoxes = new CheckBoxArray(this);
            _radioButtons = new RadioButtonArray(this);
            for (int i = 0; i < PlayerCount; i++)
            {
                var chk = _checkBoxes.AddNewCheckbox(0, 0) ;
                var radio = _radioButtons.AddNewRadioButton(0, 0);
                if (i % 2 == 0) chk.Checked = true;
                chk.AutoSize = true;
                radio.AutoSize = true;
                
            }
            _radioButtons[0].Checked = true;
        }
        public void AdvanceDealerButton()
        {
            bool buttonMoved = false;
            int startingPoint = DealerSeatNumber == PlayerCount - 1 ? 0 : DealerSeatNumber + 1;
            for (int i = startingPoint; i < PlayerCount; i++)
            {
                if (PlayerIsToBeDealtIn(i))
                {
                    _radioButtons[i].Checked = true;
                    buttonMoved = true;
                    return;
                }
            }
            if (!buttonMoved)
            {
                for (int j = 0; j < DealerSeatNumber; j++)
                {
                    if (PlayerIsToBeDealtIn(j))
                    {
                        _radioButtons[j].Checked = true;
                    }
                }
            }
        }
        public void HighlightSeat(int index)
        {
            //clear all
            for (int i = 0; i < _checkBoxes.Count; i++)
                _checkBoxes[i].BackColor = _checkBoxes[i].Parent.BackColor;

            _checkBoxes[index].BackColor = Color.Green;
        }
        public bool PlayerIsToBeDealtIn(int seatPosition)
        {
            return _checkBoxes[seatPosition].Checked;
        }
        public int DealerSeatNumber
        {
            get { return _radioButtons.SelectedIndex; }
            
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            int margin = 30;
            Rectangle ellipse = new Rectangle(margin, margin, this.Width-margin*2, this.Height-margin*2);

            
            //0 - 12 o'clock
            _checkBoxes[0].Left = this.Width / 2;
            _checkBoxes[0].Top = margin - _checkBoxes[0].Height; 
            //1 - upper right
            _checkBoxes[1].Left = this.Width / 4 *3;
            _checkBoxes[1].Top = Convert.ToInt32(this.Height * 0.09);
            //2 - 3 o'oclock
            _checkBoxes[2].Left = this.Width - margin;// checkBoxes[2].Height / 2;
            _checkBoxes[2].Top = this.Height / 2 - _checkBoxes[2].Height/2;
            //3 - lower right
            _checkBoxes[3].Left = _checkBoxes[1].Left;
            _checkBoxes[3].Top = Convert.ToInt32(this.Height - this.Height * 0.17);
            //4 - 6 o'clock
            _checkBoxes[4].Left = _checkBoxes[0].Left;
            _checkBoxes[4].Top = Convert.ToInt32(this.Height - margin);

            //5 - lower left
            _checkBoxes[5].Left = this.Width / 4;
            _checkBoxes[5].Top = _checkBoxes[3].Top;
            _checkBoxes[5].CheckAlign = ContentAlignment.TopLeft;
            _checkBoxes[5].TextAlign = ContentAlignment.BottomLeft;
            //6 - 9 o'clcok
            _checkBoxes[6].Left = 0;
            _checkBoxes[6].Top = _checkBoxes[2].Top;
            _checkBoxes[6].CheckAlign= ContentAlignment.MiddleRight;
            _checkBoxes[6].TextAlign = ContentAlignment.MiddleLeft;




            //7 - upper left
            _checkBoxes[7].Left = _checkBoxes[5].Left;
            _checkBoxes[7].Top = _checkBoxes[1].Top;
            

            //0
            _radioButtons[0].Top = _checkBoxes[0].Top + _checkBoxes[0].Height;
            _radioButtons[0].Left = _checkBoxes[0].Left;

            //1
            _radioButtons[1].Top = _checkBoxes[1].Top + _checkBoxes[0].Height;
            _radioButtons[1].Left = _checkBoxes[1].Left;

            //2
            _radioButtons[2].Top = _checkBoxes[2].Top;
            _radioButtons[2].Left = _checkBoxes[2].Left - _radioButtons[2].Width;
            

            //3
            _radioButtons[3].Top = _checkBoxes[3].Top - _checkBoxes[0].Height;
            _radioButtons[3].Left = _checkBoxes[3].Left;

            //4
            _radioButtons[4].Top = _checkBoxes[4].Top - _checkBoxes[0].Height;
            _radioButtons[4].Left = _checkBoxes[4].Left;
            //5
            _radioButtons[5].Top = _checkBoxes[5].Top - _checkBoxes[0].Height;
            _radioButtons[5].Left = _checkBoxes[5].Left;
            //6
            _radioButtons[6].Top = _checkBoxes[6].Top ;
            _radioButtons[6].Left = _checkBoxes[6].Right;
            //7
            _radioButtons[7].Top = _checkBoxes[7].Bottom;
            _radioButtons[7].Left = _checkBoxes[7].Left;
            e.Graphics.DrawEllipse(Pens.Black, ellipse);
            base.OnPaint(e);
            
        }
    }


}
