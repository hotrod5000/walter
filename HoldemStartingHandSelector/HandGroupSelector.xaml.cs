﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bot.Core.Interfaces;
using Bot.Core.Objects;
using Bot.Core;

namespace HoldemStartingHandSelector
{
    /// <summary>
    /// Interaction logic for HandGroupSelector.xaml
    /// </summary>
    public partial class HandGroupSelector : Window
    {
        List<UIElement> _controlsToTrack = new List<UIElement>();
        Bot.Core.Objects.HandCollection _playableHands = new Bot.Core.Objects.HandCollection(new HandGroupPreference());
        bool _isGlobal;
        HandCollection _cells = new HandCollection();
        public HandGroupSelector()
        {
            InitializeComponent();
            
        }
        public HandGroupSelector(Bot.Core.Objects.HandCollection hands, bool isGlobal)
            : this()
        {
            _isGlobal = isGlobal;
            _playableHands = hands;
        }
        private void PopulateGrid()
        {
            for (int i = 1; i < 14; i++)
            {
                for (int j = 1; j < 14; j++)
                {
                    _cells[i, j].GroupNumber = _playableHands.GetGroupNumber(15 - i, 15 - j, i < j);
                }
            }
        }
        static string GetHeader(int item)
        {
            var transposed = 15 - item;
            switch (transposed)
            {
                case 11:
                    return "J";
                case 12:
                    return "Q";
                case 13:
                    return "K";
                case 14:
                    return "A";
                default:
                    return transposed.ToString();

            }
        }
        void LabelRowAndColumnHeadersOnScreen()
        {
            Brush color = Brushes.Red;
            for (int i = 1; i < _cells.Size; i++)
            {
                Viewbox vbRow = new Viewbox();
                TextBlock rowHeader = new TextBlock();
                vbRow.Child = rowHeader;
                rowHeader.TextAlignment = TextAlignment.Center;
                rowHeader.Foreground = color;

                rowHeader.Text = GetHeader(i);
                Grid.SetColumn(vbRow, i);
                Grid.SetRow(vbRow, 0);
                mainGrid.Children.Add(vbRow);

                Viewbox vbCol = new Viewbox();
                TextBlock colHeader = new TextBlock();
                vbCol.Child = colHeader;
                colHeader.TextAlignment = TextAlignment.Center;
                colHeader.Foreground = color;

                colHeader.Text = GetHeader(i);
                Grid.SetColumn(vbCol, 0);
                Grid.SetRow(vbCol, i);
                mainGrid.Children.Add(vbCol);
            }
        }
        private void PlaceSuitedLabel()
        {
            //suited top right
            TextBlock b = new TextBlock();
            b.Text = "Suited";
            b.FontSize = 18;
            b.Foreground = Brushes.Orange;
            Grid.SetColumn(b, 10);
            Grid.SetRow(b, 3);
            Grid.SetColumnSpan(b, 4);
            mainGrid.Children.Add(b);

            //unsuited bottom left
            TextBlock b2 = new TextBlock();
            b2.Text = "Not suited";
            b2.FontSize = 18;
            b2.Foreground = Brushes.Orange;
            Grid.SetColumn(b2, 3);
            Grid.SetRow(b2, 10);
            Grid.SetColumnSpan(b2, 4);
            mainGrid.Children.Add(b2);

        }
        private void InitializeGrid()
        {
            for (int i = 0; i < _cells.Size; i++)
            {
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
                mainGrid.RowDefinitions.Add(new RowDefinition());
            }
            PlaceSuitedLabel();
            LabelRowAndColumnHeadersOnScreen();
            for (int row = 1; row < _cells.Size; row++)
            {
                for (int column = 1; column < _cells.Size; column++)
                {
                    Border brdr = new Border();
                    DockPanel b = new DockPanel() { Background = Brushes.Transparent };
                    brdr.Child = b;
                    brdr.BorderBrush = Brushes.Gray;
                    brdr.BorderThickness = new Thickness(0.2);
                    //Button butt = new Button() { Content = "sh" };
                    Viewbox vb = new Viewbox();
                    //DockPanel p = new DockPanel();
                    //p.LastChildFill = true;
                    //p.Background = Brushes.Green;
                    Label txtBlock = new Label();
                    b.Children.Add(vb);
                    //b.Child = vb;
                    vb.Child = txtBlock;
                    //p.Children.Add(txtBlock);
                    Grid.SetColumn(brdr, column);
                    Grid.SetRow(brdr, row);
                    Grid.SetColumn(b, column);
                    Grid.SetRow(b, row);
                    _controlsToTrack.Add(b);
                    txtBlock.DataContext = _cells[row, column];
                    mainGrid.Children.Add(brdr);
                    txtBlock.Style = Resources["lifeStyle"] as Style;
                    
                    b.MouseDown += new MouseButtonEventHandler(b_MouseDown);
                    b.MouseEnter += new MouseEventHandler(b_MouseEnter);
                }
            }
            
        }

        int GetDepressedNumberKey()
        {
            int numberKey = 0;
            if (Keyboard.IsKeyDown(Key.D1) || Keyboard.IsKeyDown(Key.NumPad1))
                numberKey = 1;
            else if (Keyboard.IsKeyDown(Key.D2) || Keyboard.IsKeyDown(Key.NumPad2))
                numberKey = 2;
            else if (Keyboard.IsKeyDown(Key.D3) || Keyboard.IsKeyDown(Key.NumPad3))
                numberKey = 3;
            else if (Keyboard.IsKeyDown(Key.D4) || Keyboard.IsKeyDown(Key.NumPad4))
                numberKey = 4;
            else if (Keyboard.IsKeyDown(Key.D5) || Keyboard.IsKeyDown(Key.NumPad5))
                numberKey = 5;
            else if (Keyboard.IsKeyDown(Key.D6) || Keyboard.IsKeyDown(Key.NumPad6))
                numberKey = 6;
            else if (Keyboard.IsKeyDown(Key.D7) || Keyboard.IsKeyDown(Key.NumPad7))
                numberKey = 7;
            else if (Keyboard.IsKeyDown(Key.D8) || Keyboard.IsKeyDown(Key.NumPad8))
                numberKey = 8;
            else if (Keyboard.IsKeyDown(Key.D9) || Keyboard.IsKeyDown(Key.NumPad9))
                numberKey = 9;

            return numberKey;
        }
        void b_MouseEnter(object sender, MouseEventArgs e)
        {
            int numberKey = GetDepressedNumberKey();

            if (e.LeftButton == MouseButtonState.Released && e.RightButton == MouseButtonState.Released) return;

            var uiElement = sender as UIElement;
            if (uiElement != null)
            {
                var row = Grid.GetRow(uiElement);
                var col = Grid.GetColumn(uiElement);
                if (row == 0 || col == 0) return;
                if (e.RightButton == MouseButtonState.Pressed)
                    _cells[row, col].GroupNumber = 0;
                else if (numberKey == 0)
                    _cells[row, col].GroupNumber++;
                else
                    _cells[row, col].GroupNumber = numberKey;
            }
        }

        void b_MouseDown(object sender, MouseButtonEventArgs e)
        {
            int numberKey = GetDepressedNumberKey();
           
            
            var uiElement = sender as UIElement;
            if (uiElement != null)
            {
                var row = Grid.GetRow(uiElement);
                var col = Grid.GetColumn(uiElement);
                if (row == 0 || col == 0) return;
                if (e.RightButton == MouseButtonState.Pressed)
                    _cells[row, col].GroupNumber = 0;
                else if (numberKey == 0)
                    _cells[row, col].GroupNumber++;
                else
                    _cells[row, col].GroupNumber = numberKey;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeGrid();
            PopulateGrid();
            
        }

        
        private void TransposeUi2CoreObject(IPlayableHands hands)
        {
            for (int i = 1; i < 14; i++)
            {
                for (int j = 1; j < 14; j++)
                {
                    hands.Set(15 - i, 15 - j, i < j, _cells.Cells[i, j].GroupNumber);
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            TransposeUi2CoreObject(_playableHands);

            if (_isGlobal)
            {
                var fileName = System.IO.Path.Combine(SerializationMethods.ConfigFilesfolderName, SerializationMethods.DefaultStartingHandFile);
                SerializationMethods.SerializeStartingHands(_playableHands as Bot.Core.Objects.HandCollection, fileName);
            }
        }

        
    }
}
