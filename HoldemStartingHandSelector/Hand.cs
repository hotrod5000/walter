﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;

namespace HoldemStartingHandSelector
{
    [Serializable]
    public class StartingHand : INotifyPropertyChanged, IValueConverter
    {
        int _groupNumber;
        public int GroupNumber
        {
            get
            {
                return _groupNumber;
            }
            set
            {
                _groupNumber = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(
                      this,
                      new PropertyChangedEventArgs("GroupNumber")
                    );
                }
            }
        }
        public bool IsPlayable
        {
            get
            {
                return _isPlayable;
            }

            set
            {
                _isPlayable = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(
                      this,
                      new PropertyChangedEventArgs("IsPlayable")
                    );
                }
            }
        }
        public override string ToString()
        {
            return _isPlayable.ToString();
        }
        private bool _isPlayable = false;

        #region INotifyPropertyChanged Members
         [field: NonSerialized]
        public event
           PropertyChangedEventHandler PropertyChanged;

        #endregion

         public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
         {
             if ((int)value == 1)
                 return "";
             return value;
         }

         public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
         {
             throw new NotImplementedException();
         }
    }
}
