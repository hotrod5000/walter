﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bot.Core.Objects;
using Bot.Core;
using Bot.Core.Interfaces;

namespace HoldemStartingHandSelector
{
    /// <summary>
    /// Interaction logic for HandConfigurator.xaml
    /// </summary>
    public partial class HandConfigurator : Window
    {
        readonly int MaxNumberOfPlayers = 10;
        bool _isGlobal;
        IHandGroupPreference _handGroupPreference = new HandGroupPreference();
        public HandConfigurator()
        {
            InitializeComponent();

        }
        public HandConfigurator(IHandGroupPreference pref, bool isGlobal):this()
        {
            _handGroupPreference = pref;
            _isGlobal = isGlobal;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeGrid();
        }
        private void InitializeGrid()
        {
            for (int i = 0; i <= MaxNumberOfPlayers; i++)
            {
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
                mainGrid.RowDefinitions.Add(new RowDefinition());
            }
            LabelColumnHeadersOnScreen();
            LabelRowHeadersOnScreen();
            for (int row = 1; row < MaxNumberOfPlayers; row++)
            {
                int numberOfPlayers = row + 1;
                for (int column = 1; column <= numberOfPlayers; column++)
                {
                    int position = column;
                    Border b = new Border();
                    b.BorderBrush = Brushes.Gray;
                    b.BorderThickness = new Thickness(0.2);
                    
                    Viewbox vb = new Viewbox();

                    TextBox txtBox = new TextBox() { Background = Brushes.Transparent };
                    txtBox.GotFocus += new RoutedEventHandler(txtBox_GotFocus);
                    b.Child = vb;
                    vb.Child = txtBox;
                    Grid.SetColumn(b, column);
                    Grid.SetRow(b, row);

                    txtBox.DataContext = _handGroupPreference[numberOfPlayers, position];
                    mainGrid.Children.Add(b);
                    txtBox.Style = Resources["lifeStyle"] as Style;

                }
            }
        }

        void txtBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var txtBox = sender as TextBox;
            if (txtBox == null) return;
            txtBox.SelectAll();
        }

        

        private void LabelColumnHeadersOnScreen()
        {
            for (int row = 1; row < MaxNumberOfPlayers; row++)
            {
                AddTextBlockToCell((row + 1).ToString(), row, 0);
            }
        }

        private void LabelRowHeadersOnScreen()
        {
            AddTextBlockToCell("# of players", 0, 0);
            AddTextBlockToCell("sb", 0, 1);
            AddTextBlockToCell("bb", 0, 2);
        }

        private void AddTextBlockToCell(string text, int row, int col)
        {
            Border b = new Border();
            b.BorderBrush = Brushes.Gray;
            b.BorderThickness = new Thickness(0.2);
            Viewbox vb = new Viewbox();

            TextBlock txtBlock = new TextBlock();
            txtBlock.Text = text;

            b.Child = vb;
            vb.Child = txtBlock;
            Grid.SetColumn(b, col);
            Grid.SetRow(b, row);

            mainGrid.Children.Add(b);
            //txtBlock.Style = Resources["lifeStyle"] as Style;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (_isGlobal)
            {
                var fileName = System.IO.Path.Combine(SerializationMethods.ConfigFilesfolderName, SerializationMethods.DefaultHandPreferencesFile);
                SerializationMethods.Serialize<IHandGroupPreference>(_handGroupPreference, fileName);
            }
        }

    }
}
