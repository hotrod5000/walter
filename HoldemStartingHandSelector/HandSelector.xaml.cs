﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Bot.Core;
using Bot.Core.Interfaces;
using Bot.Core.Enumerations;
using Bot.Core.Objects;


namespace HoldemStartingHandSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class HandSelector : Window
    {
        HandCollection _cells = new HandCollection();
        IPlayableHands _hands = new Bot.Core.Objects.HandCollection(new HandGroupPreference());
        public HandSelector()
        {
            InitializeComponent();


        }
        public HandSelector(IPlayableHands hands)
            : this()
        {

            _hands = hands;

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            InitializeGrid();
            PopulateGrid();
        }
        private void PopulateGrid()
        {
            for (int i = 1; i < 14; i++)
            {
                for (int j = 1; j < 14; j++)
                {
                    _cells[i, j].IsPlayable = _hands.Get(15 - i, 15 - j, i < j);
                }
            }
        }
        private void PlaceSuitedLabel()
        {
            //suited top right
            TextBlock b = new TextBlock();
            b.Text = "Suited";
            b.FontSize = 18;
            b.Foreground = Brushes.Orange;
            Grid.SetColumn(b, 10);
            Grid.SetRow(b, 3);
            Grid.SetColumnSpan(b, 4);
            mainGrid.Children.Add(b);

            //unsuited bottom left
            TextBlock b2 = new TextBlock();
            b2.Text = "Not suited";
            b2.FontSize = 18;
            b2.Foreground = Brushes.Orange;
            Grid.SetColumn(b2, 3);
            Grid.SetRow(b2, 10);
            Grid.SetColumnSpan(b2, 4);
            mainGrid.Children.Add(b2);

        }
        void LabelRowAndColumnHeadersOnScreen()
        {
            Brush color = Brushes.Red;
            for (int i = 1; i < _cells.Size; i++)
            {
                Viewbox vbRow = new Viewbox();
                TextBlock rowHeader = new TextBlock();
                rowHeader.TextAlignment = TextAlignment.Center;
                rowHeader.Foreground = color;

                rowHeader.Text = GetHeader(i);
                Grid.SetColumn(rowHeader, i);
                Grid.SetRow(rowHeader, 0);
                mainGrid.Children.Add(rowHeader);

                Viewbox vbCol = new Viewbox();
                TextBlock colHeader = new TextBlock();
                vbCol.Child = colHeader;
                colHeader.TextAlignment = TextAlignment.Center;
                colHeader.Foreground = color;

                colHeader.Text = GetHeader(i);
                Grid.SetColumn(vbCol, 0);
                Grid.SetRow(vbCol, i);
                mainGrid.Children.Add(vbCol);
            }
        }
        static string GetHeader(int item)
        {
            var transposed = 15 - item;
            switch (transposed)
            {
                case 11:
                    return "J";
                case 12:
                    return "Q";
                case 13:
                    return "K";
                case 14:
                    return "A";
                default:
                    return transposed.ToString();

            }
        }
        private void InitializeGrid()
        {
            for (int i = 0; i < _cells.Size; i++)
            {
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
                mainGrid.RowDefinitions.Add(new RowDefinition());
            }
            PlaceSuitedLabel();
            LabelRowAndColumnHeadersOnScreen();
            for (int row = 0; row < _cells.Size; row++)
            {
                for (int column = 0; column < _cells.Size; column++)
                {

                    Border b = new Border();
                    b.BorderBrush = Brushes.Gray;
                    b.BorderThickness = new Thickness(0.2);
                    Ellipse ellipse = new Ellipse();
                    b.Child = ellipse;
                    Grid.SetColumn(b, column);
                    Grid.SetRow(b, row);

                    ellipse.DataContext = _cells[row, column];
                    mainGrid.Children.Add(b);
                    ellipse.Style = Resources["lifeStyle"] as Style;
                    b.MouseDown += new MouseButtonEventHandler(b_MouseDown);
                    b.MouseEnter += new MouseEventHandler(b_MouseEnter);
                }
            }
        }

        void b_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released) return;
            var uiElement = sender as UIElement;
            if (uiElement != null)
            {
                var row = Grid.GetRow(uiElement);
                var col = Grid.GetColumn(uiElement);
                if (row == 0 || col == 0) return;
                _cells[row, col].IsPlayable = !_cells[row, col].IsPlayable;
            }
        }

        void b_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var uiElement = sender as UIElement;
            if (uiElement != null && e.LeftButton == MouseButtonState.Pressed)
            {
                var row = Grid.GetRow(uiElement);
                var col = Grid.GetColumn(uiElement);
                if (row == 0 || col == 0) return;
                _cells[row, col].IsPlayable = !_cells[row, col].IsPlayable;


            }
        }

        private void SaveAsDefault_Click(object sender, RoutedEventArgs e)
        {
            Bot.Core.Objects.HandCollection hands = new Bot.Core.Objects.HandCollection(new HandGroupPreference());
            TransposeUi2CoreObject(hands);

            var fileName = System.IO.Path.Combine(SerializationMethods.ConfigFilesfolderName, SerializationMethods.DefaultStartingHandFile);
            SerializationMethods.SerializeStartingHands(hands, fileName);
        }

        private void TransposeUi2CoreObject(IPlayableHands hands)
        {
            for (int i = 1; i < 14; i++)
            {
                for (int j = 1; j < 14; j++)
                {
                    hands.Set(15 - i, 15 - j, i < j, _cells.Cells[i, j].IsPlayable ? 1 : 0);
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            TransposeUi2CoreObject(_hands);
        }



    }
}
