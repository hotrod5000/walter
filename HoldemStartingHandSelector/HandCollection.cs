﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Core.Interfaces;

namespace HoldemStartingHandSelector
{
    [Serializable]
    public class HandCollection
    {
        public int Size { get { return 14; } }
        public HandCollection()
        {
            int size = 14;
            
            for (int row = 0; row < size; row++)
            {
                for (int column = 0; column < size; column++)
                {
                    _cells[row,column] = new StartingHand();
                }
            }
        }

        public StartingHand this[int row, int column]
        {
            get
            {
                return _cells[row,column];
            }
        }
        




        private StartingHand[,] _cells = new StartingHand[14, 14];

        public StartingHand[,] Cells
        {
            get { return _cells; }
            set { _cells = value; }
        }
    }
}
