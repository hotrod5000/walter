﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;
using EasyHook;
using Bot.UI;



namespace Injectable.PokerStars.WriteFileHook
{
    
    public class Main : EasyHook.IEntryPoint
    {
        const string Marker = "MSG_TABLE_LOGIN_REPLY";
        HookHandler Interface;
        LocalHook WriteFileHook;
        Dictionary<IntPtr, Queue<String>> Queue = new Dictionary<IntPtr, Queue<string>>();
        

        public Main(
            RemoteHooking.IContext InContext,
            String InChannelName)
        {
            // connect to host...
            Interface = RemoteHooking.IpcConnectClient<HookHandler>(InChannelName);

            Interface.Ping();
        }

        public void Run(
            RemoteHooking.IContext InContext,
            String InChannelName)
        {
            // install hook...
            try
            {
                WriteFileHook = LocalHook.Create(
                    LocalHook.GetProcAddress("kernel32.dll", "WriteFile"),
                    new DWriteFile(WriteFile_Hooked),
                    this);

                WriteFileHook.ThreadACL.SetExclusiveACL(new Int32[] { 0 });

            }
            catch (Exception ExtInfo)
            {
                Exception myException = new Exception("failed to hook 'WriteFile'", ExtInfo);
                Interface.ReportException(myException);

                return;
            }

            Interface.IsInstalled(RemoteHooking.GetCurrentProcessId());
            RemoteHooking.WakeUpProcess();

            // wait for host process termination...
            try
            {
                while (true)
                {
                    Thread.Sleep(500);

                    // transmit newly monitored file accesses...
                    if (Queue.Count > 0)
                    {
                        String[] Package = null;

                        lock (Queue)
                        {
                            foreach (var item in Queue)
                            {
                                Package = item.Value.ToArray();
                                Interface.OnFileWrite(RemoteHooking.GetCurrentProcessId(),item.Key, Package);
                                item.Value.Clear();
                            }
                        }

                        
                    }
                    else
                        Interface.Ping();
                }
            }
            catch(Exception ex)
            {
                Interface.ReportException(ex);
                Console.WriteLine("Done: " + ex.Message);
            }
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall,
            CharSet = CharSet.Ansi,
            SetLastError = true)]
        delegate bool DWriteFile(
                            IntPtr hFile,
                            System.Text.StringBuilder lpBuffer,
                            uint nNumberOfBytesToWrite,
                            out uint lpNumberOfBytesWritten,
                            [In] ref System.Threading.NativeOverlapped lpOverlapped);

        
        // just use a P-Invoke implementation to get native API access from C# (this step is not necessary for C++.NET)
        [DllImport("kernel32.dll", BestFitMapping = true, CharSet = CharSet.Ansi)]
        static extern bool WriteFile(IntPtr hFile,
                            System.Text.StringBuilder lpBuffer,
                            uint nNumberOfBytesToWrite,
                            out uint lpNumberOfBytesWritten,
                            [In] ref System.Threading.NativeOverlapped lpOverlapped);





        // this is where we are intercepting all file accesses!
        static bool WriteFile_Hooked(
                            IntPtr hFile,
                            System.Text.StringBuilder lpBuffer,
                            uint nNumberOfBytesToWrite,
                            out uint lpNumberOfBytesWritten,
                            [In] ref System.Threading.NativeOverlapped lpOverlapped)
        {

            try
            {

                Main This = (Main)HookRuntimeInfo.Callback;
                string sensableBuffer = lpBuffer.ToString().Substring(0, (int)nNumberOfBytesToWrite).Replace("\r\n", " ");
                lock (This.Queue)
                {
                    if (!This.Queue.ContainsKey(hFile))
                        This.Queue[hFile] = new Queue<string>();
                    This.Queue[hFile].Enqueue(sensableBuffer);

                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // call original API...
            return WriteFile(
                            hFile,
                            lpBuffer,
                            nNumberOfBytesToWrite,
                            out lpNumberOfBytesWritten,
                            ref lpOverlapped);
        }

        
    }
}
