﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Injectable.PokerStars.WriteFileHook
{
    public struct RECT
    {
        public int Left, Top, Right, Bottom;
        public RECT(Rectangle r) 
        {
            this.Left = r.Left;
            this.Top = r.Top;
            this.Bottom = r.Bottom;
            this.Right = r.Right;
        }
    }

}
